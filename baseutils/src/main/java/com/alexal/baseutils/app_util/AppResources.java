package com.alexal.baseutils.app_util;

import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;

/**
 * Class containing some helper methods for fast access to the application resources.
 * CAUTION: This class is tight coupled with {@link ContextReference} class. So
 * if you use it, do not forget to {@link ContextReference#wrap(Context)} the application
 * context on your overridden Application class.
 */
public class AppResources {

	private AppResources(){}

	public static String getString(@StringRes int stringValue){
		return ContextReference.appContext().getResources().getString(stringValue);
	}

	public static int getColor(@ColorRes int colorResource){
		return ContextCompat.getColor(
				ContextReference.appContext(),
				colorResource
		);
	}


}
