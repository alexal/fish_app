package com.alexal.baseutils.app_util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;


/**
 * Util class providing methods for network access.
 */
public class Network {

	private Network(){}


	/**
	 * Checks if wifi or cellular data is currently available on the device.
	 * Uses a given context reference to get the connection service
	 * @param context
	 * @return true if available,otherwise false.
	 */
	public static boolean wifiOrCellularAvailable(@NonNull Context context){

		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo info = cm.getActiveNetworkInfo();

		return  (info != null && info.isConnectedOrConnecting());
	}

	/**
	 * Checks if wifi or cellular data is currently available on the device.
	 * WARNING: To use this method you must have wrapped an application context
	 * using the {@link ContextReference#wrap(Context)} method.
	 * @return true if available,otherwise false.
	 */
	public static boolean wifiOrCellularAvailable(){
		return wifiOrCellularAvailable(ContextReference.appContext());
	}
}
