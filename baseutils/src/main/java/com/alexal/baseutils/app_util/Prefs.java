package com.alexal.baseutils.app_util;


import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

public class Prefs {

	private static final String PREF_FILE = "PREF_FILE";

	public static final String PREF_RECENT_VIEWED = "PREF_RECENT_VIEWED";

	private Prefs(){}

	public static SharedPreferences prefs(@NonNull Context context){
		return context.getSharedPreferences(PREF_FILE,Context.MODE_PRIVATE);
	}

	public static SharedPreferences prefs(){
		return ContextReference.appContext()
							   .getSharedPreferences(PREF_FILE,Context.MODE_PRIVATE);
	}

	public static SharedPreferences.Editor edit(@NonNull Context context){
		return prefs(context).edit();
	}

	public static SharedPreferences.Editor edit(){
		return prefs().edit();
	}

	public static void putBoolean(@NonNull Context context,
								  String key,
								  boolean value){

		edit(context).putBoolean(key,value).apply();
	}

	public static void putBoolean(String key,
								  boolean value){

		edit().putBoolean(key,value).apply();
	}


}
