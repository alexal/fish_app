package com.alexal.baseutils.app_util;


import android.content.Context;
import android.support.annotation.NonNull;

public class ContextReference {

	private static Context appContext;

	public static void wrap(@NonNull Context applicationContext){
		appContext = applicationContext;
	}

	public static Context appContext(){
		if (appContext != null)
		 	return appContext;
		else
			throw new IllegalArgumentException("App context not wrapped.Call wrap() method first");
	}

}
