package com.alexaldev.fisher.home.cart_view;

/**
 *
 * Created by alexal on 19/11/2017.
 */
public class DeleteFromCartEvent {

	private final String fishName;

	public DeleteFromCartEvent(String fishName) {

		this.fishName = fishName;
	}

	public String getFishName() {

		return fishName;
	}
}
