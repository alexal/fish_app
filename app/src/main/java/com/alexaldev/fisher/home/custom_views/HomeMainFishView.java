package com.alexaldev.fisher.home.custom_views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alexalbasemvp.mvp.BUS;
import com.alexaldev.fisher.R;
import com.alexaldev.fisher.home.FishTitleClickEvent;

/**
 * The custom view class of a fish entry. Useful because we use the same
 * view on many home places.
 */

public class HomeMainFishView extends LinearLayout {

	LinearLayout root;
	TextView titleView;

	public HomeMainFishView(Context context,
							@Nullable AttributeSet attrs) {

		super(context,
			  attrs);

		root = (LinearLayout) LayoutInflater.from(context)
											.inflate(R.layout.view_fish_entry_home,this,true);

		titleView = root.findViewById(R.id.tv_fentry_home_title);

		root.setOnClickListener(v -> BUS.postEvent(
				new FishTitleClickEvent(titleView.getText().toString())
		));
	}

	public void setTitle(String title){
		titleView.setText(title);
	}
}
