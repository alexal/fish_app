package com.alexaldev.fisher.home.main_view;

import com.alexaldev.fisher.domain.dataModel.Fish;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexal on 15/11/2017.
 */
@Deprecated
public class MainHomeModelParser{

	List<String> parseFrom(List<Fish> data){

		List<String> result = new ArrayList<>();

		for (Fish f : data)
			result.add(f.getName());

		return result;
	}

}
