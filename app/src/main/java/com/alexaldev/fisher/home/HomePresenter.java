package com.alexaldev.fisher.home;

import android.content.Intent;
import android.support.annotation.NonNull;

import com.alexalbasemvp.mvp.BasePresenter;
import com.alexaldev.fisher.domain.dao.FishDAO;
import com.alexaldev.fisher.domain.repository.local.CartUpdateEvent;
import com.alexaldev.fisher.domain.repository.local.FishLocalRepository;
import com.alexaldev.fisher.fish_presentation.FPresentationPresenter;
import com.alexaldev.fisher.fish_presentation.FishPresentationController;

import org.greenrobot.eventbus.Subscribe;

public class HomePresenter extends BasePresenter<HomeController>
		implements Contract.PresenterOps {

	private static final String TAG = "HomePresenter";

	@Override
	protected boolean subscribesToEventBus() {

		return true;
	}

	@Override
	protected void onAttach(@NonNull HomeController view) {

		FishLocalRepository.configureLocalData();
//		RemoteRepository.uploadFishEntries();

		getView().navigateToView(HomeController.MAIN_VIEW);

	}

	@Override
	protected void onComingIntent(Intent intent) {

		super.onComingIntent(intent);


		if (intent.hasExtra(HomeController.OPEN_SPECIFIC_VIEW)){

			int viewID = intent.getIntExtra(HomeController.OPEN_SPECIFIC_VIEW,
											HomeController.MAIN_VIEW);

			getView().navigateToView(viewID);
		}

	}

	@Override
	protected void onDetach() {

	}


	@Override
	public void onNavigationViewSelected(int viewID) {
		getView().loadView(viewID);
	}


	@Override
	@Subscribe
	public void onViewLoaded(ViewLoadedEvent loadEvent) {
		switch (loadEvent.getViewId()){

			case HomeController.MAIN_VIEW:
				getView().showRecentSearches(
						FishDAO.getInstance().getRecentViewed()
				);
				break;
			case HomeController.CART_VIEW:
				//Must query for the cart entries
				if (FishDAO.getInstance()
						.getAllFishInCart().size() > 0){

					getView().showCart(
							FishDAO.getInstance().getAllFishInCart()
					);

				}
				else {
					getView().showNoCartEntries();
				}

				break;

			default:
				getView().showMessage("View coming soon");


		}
	}

	@Override
	@Subscribe
	public void onFishEntriesClick(FishEntriesClickEvent event) {
		getView().openFishEntriesScreen();
	}


	@Subscribe
	@Override
	public void onFishClick(FishTitleClickEvent event){

		Intent fishPresentationIntent = new Intent(getView(),
												   FishPresentationController.class);

		fishPresentationIntent.putExtra(
				FPresentationPresenter.FISH_KEY,event.getFishName()
		);

		getView().startActivity(fishPresentationIntent);

	}

	@Subscribe
	@Override
	public void onCartUpdate(CartUpdateEvent event) {

		//Should check if there is still fish in the cart
		if (FishDAO.getInstance().cartEntriesExist())
			getView().updateCart(
					FishDAO.getInstance().getAllFishInCart()
			);
		else {
			getView().showNoCartEntries();
		}
	}

}
