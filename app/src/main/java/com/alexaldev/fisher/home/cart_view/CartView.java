package com.alexaldev.fisher.home.cart_view;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.alexaldev.fisher.R;
import com.alexaldev.fisher.domain.dataModel.FishInCart;
import com.alexaldev.fisher.home.BaseHomeView;
import com.alexaldev.fisher.home.HomeController;
import com.alexaldev.fisher.home.custom_views.CartNoEntriesView;

import java.util.List;

import butterknife.BindView;


public class CartView extends BaseHomeView {

	@BindView(R.id.cart_no_entries_view)
	protected CartNoEntriesView cartNoEntriesView;

	@BindView(R.id.rv_cart_entries)
	protected RecyclerView rvCartEntries;

	private CartViewAdapter mAdapter;

	public static CartView newInstance() {

		
		Bundle args = new Bundle();
		
		CartView fragment = new CartView();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	protected void initViews(View root) {

		super.initViews(root);



	}

	public void showNoEntriesFound(){
		this.cartNoEntriesView.setVisibility(View.VISIBLE);
		this.rvCartEntries.setVisibility(View.GONE);
	}

	public void showFishEntries(List<FishInCart> fishInCart){

		Log.d("CartView","Showing fish entries...");

		this.cartNoEntriesView.setVisibility(View.GONE);
		this.rvCartEntries.setVisibility(View.VISIBLE);

		rvCartEntries.setLayoutManager(new LinearLayoutManager(getContext(),
															   LinearLayoutManager.VERTICAL,
															   false));
		rvCartEntries.addItemDecoration(new DividerItemDecoration(getContext(),
																  DividerItemDecoration.VERTICAL));

		this.mAdapter = new CartViewAdapter(getContext(),
											fishInCart);

		this.rvCartEntries.setAdapter(mAdapter);
	}

	public void updateCart(List<FishInCart> newData){
		this.mAdapter.updateCartEntries(newData);
	}


	@Override
	protected int getViewID() {

		return HomeController.CART_VIEW;
	}

	@Override
	protected int getLayoutId() {

		return R.layout.view_home_cart;
	}

	@Nullable
	@Override
	protected String getToolbarTitle() {

		return null;
	}

}
