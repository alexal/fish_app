package com.alexaldev.fisher.home;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.alexalbasemvp.mvp.BaseController;
import com.alexaldev.fisher.R;
import com.alexaldev.fisher.domain.dataModel.FishInCart;
import com.alexaldev.fisher.fish_entries.FishEntriesController;
import com.alexaldev.fisher.home.account_view.AccountView;
import com.alexaldev.fisher.home.cart_view.CartView;
import com.alexaldev.fisher.home.main_view.MainHomeView;
import com.alexaldev.fisher.home.settings_view.SettingsView;

import java.util.List;

import butterknife.BindView;


public class HomeController extends BaseController<HomeController,HomePresenter>
		implements Contract.ControllerOps,
				   BottomNavigationView.OnNavigationItemSelectedListener {

	@BindView(R.id.bottom_nav_home)
	protected BottomNavigationView navigationView;

	private static final int FRAGMENT_CONTAINER = R.id.fl_home;

	private int currentOpenView;

	//Sub views
	private SettingsView mSettingsView;
	private MainHomeView mMainHomeView;
	private CartView mCartView;
	private AccountView mAccountView;


	public static final String OPEN_SPECIFIC_VIEW = "SPECIFIC_VIEW";
	public static final int MAIN_VIEW = 111;
	public static final int ACCOUNT_VIEW = 112;
	public static final int CART_VIEW = 113;
	public static final int SETTINGS_VIEW = 114;

	@Override
	protected void initViews() {

		navigationView.setOnNavigationItemSelectedListener(this);
	}

	@Override
	protected HomeController getThis() {

		return this;
	}

	@Override
	protected boolean preservePresenter() {

		return true;
	}

	@Override
	protected HomePresenter newPresenter() {

		return new HomePresenter();
	}


	@Override
	protected int getLayoutId() {

		return R.layout.activity_home_view;
	}

	@Nullable
	@Override
	protected Toolbar getToolbar() {

		return null;
	}

	@Override
	protected String getToolbarTitle() {

		return null;
	}

	@Override
	protected boolean homeAsUpEnabled() {

		return false;
	}

	@Override
	public void showMessage(String message) {

		Toast.makeText(this,
					   message,
					   Toast.LENGTH_SHORT)
			 .show();
	}

	@Override
	public boolean onNavigationItemSelected(@NonNull MenuItem item) {

		Log.d("HomeController","navigationSelected");

		switch (item.getItemId()){

			case R.id.menu_home_home:
				getPresenter().onNavigationViewSelected(MAIN_VIEW);
				break;
			case R.id.menu_home_cart:
				getPresenter().onNavigationViewSelected(CART_VIEW);
				break;
//			case R.id.menu_home_account:
//				getPresenter().onNavigationViewSelected(ACCOUNT_VIEW);
//				break;
			case R.id.menu_home_settings:
				getPresenter().onNavigationViewSelected(SETTINGS_VIEW);
				break;
			default:
				throw new IllegalArgumentException("How is this even possible");
		}

		return true;
	}

	@Override
	public  int getFragmentContainer() {

		return FRAGMENT_CONTAINER;
	}

	@Override
	public void openFishEntriesScreen() {
		startActivity(
				new Intent(this,
						   FishEntriesController.class)
		);
	}

	@Override
	public void navigateToView(int viewID) {

		int navItemID;

		switch (viewID){
			case MAIN_VIEW:
				navItemID = R.id.menu_home_home;
				loadView(MAIN_VIEW);
				break;
			case SETTINGS_VIEW:
				navItemID = R.id.menu_home_settings;
				loadView(SETTINGS_VIEW);
				break;
			case CART_VIEW:
				navItemID = R.id.menu_home_cart;
				loadView(CART_VIEW);
				break;
			default:
				navItemID = R.id.menu_home_home;
				loadView(MAIN_VIEW);
		}

		this.navigationView.setSelectedItemId(navItemID);
	}

	public void loadView(int viewID) {
		switch (viewID){
			case MAIN_VIEW:
				this.showMainView();
				break;
			case SETTINGS_VIEW:
				this.showSettingsView();
				break;
			case CART_VIEW:
				this.showCartView();
				break;
			default:
				showMessage("View is coming soon...");

		}
	}

	@Override
	public int getOpenViewID() {

		return this.currentOpenView;
	}

	private void showMainView(){

		if (this.mMainHomeView == null){
			this.mMainHomeView = MainHomeView.newInstance();
		}

		this.currentOpenView = MAIN_VIEW;


		replaceTransaction(mMainHomeView);
	}

	private void showCartView(){
		if (this.mCartView == null){
			this.mCartView = CartView.newInstance();
		}

		this.currentOpenView = CART_VIEW;

		replaceTransaction(mCartView);
	}

	private void showAccountView(){
		//TODO
	}

	private void showSettingsView(){
		if (mSettingsView == null)
			mSettingsView = SettingsView.newInstance();

		this.currentOpenView = SETTINGS_VIEW;

		getSupportFragmentManager().beginTransaction()
								   .replace(FRAGMENT_CONTAINER,mSettingsView)
								   .commit();
	}

	@Override
	public void showRecentSearches(List<String> recentSearches) {
		mMainHomeView.showRecentViewed(recentSearches);
	}

	@Override
	public void showNoCartEntries() {
		this.mCartView.showNoEntriesFound();
	}

	@Override
	public void showCart(List<FishInCart> data) {
		this.mCartView.showFishEntries(data);
	}

	@Override
	public void updateCart(List<FishInCart> newData) {
		mCartView.updateCart(newData);
	}
}
