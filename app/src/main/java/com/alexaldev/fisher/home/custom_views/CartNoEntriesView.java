package com.alexaldev.fisher.home.custom_views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.alexalbasemvp.mvp.BUS;
import com.alexaldev.fisher.R;
import com.alexaldev.fisher.home.FishEntriesClickEvent;

/**
 * Created by alexal on 18/11/2017.
 */

public class CartNoEntriesView extends RelativeLayout {

	RelativeLayout root;

	public CartNoEntriesView(Context context, AttributeSet attrs) {

		super(context,
			  attrs);

		this.init(context);
	}

	private void init(Context context) {
		root = (RelativeLayout) LayoutInflater.from(context)
											  .inflate(R.layout.view_home_cart_no_entries,this,true);

		ImageView addCart  = root.findViewById(R.id.iv_cart_add);

		addCart.setOnClickListener(v -> BUS.postEvent(new FishEntriesClickEvent()));
	}
}
