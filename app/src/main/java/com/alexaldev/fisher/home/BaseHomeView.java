package com.alexaldev.fisher.home;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.alexalbasemvp.mvp.BaseView;
import com.alexalbasemvp.mvp.BUS;

/**
 *
 * Created by alexal on 16/11/2017.
 */

public abstract class BaseHomeView extends BaseView<HomePresenter> {

	@Override
	public Resources appResources() {

		return getResources();
	}

	@Override
	public void showMessage(String message) {

		Toast.makeText(getContext(),
					   message,
					   Toast.LENGTH_SHORT)
			 .show();
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

		super.onViewCreated(view,
							savedInstanceState);

		BUS.postEvent(new ViewLoadedEvent(getViewID()));
	}

	/**
	 * Must be one of the HomeController static final IDs
	 * @return
	 */
	protected abstract int getViewID();
}
