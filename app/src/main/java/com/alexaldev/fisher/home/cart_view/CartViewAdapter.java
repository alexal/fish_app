package com.alexaldev.fisher.home.cart_view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alexalbasemvp.mvp.BUS;
import com.alexaldev.fisher.R;
import com.alexaldev.fisher.domain.dataModel.FishInCart;
import com.alexaldev.fisher.home.FishTitleClickEvent;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

/**
 * Created by alexal on 18/11/2017.
 */
public class CartViewAdapter extends RecyclerView.Adapter<CartViewAdapter.FishInCartViewHolder> {

	private final String ADDED_PREFIX = "Προστέθηκε στις: ";

	private List<FishInCart> fishData;
	private Context context;

	public CartViewAdapter(@NonNull Context context,
						   @NonNull List<FishInCart> fishData) {

		this.context = context;
		this.fishData = fishData;
	}

	@Override
	public FishInCartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

		return new FishInCartViewHolder(
				LayoutInflater.from(parent.getContext())
							  .inflate(R.layout.view_cart_entry,
									   parent,
									   false)
		);

	}

	@Override
	public void onBindViewHolder(FishInCartViewHolder holder, int position) {


		holder.fishName.setText(fishData.get(position)
										.getFishName());

		holder.addedDate.setText(String.format("%s%s",
											   ADDED_PREFIX,
											   fishData.get(position)
													   .getAddedDate()));
		Glide.with(context)
			 .load(fishData.get(position)
						   .getImageUri())
			 .apply(RequestOptions.bitmapTransform(new CircleCrop()))
			 .into(holder.fishImage);

	}


	@Override
	public int getItemCount() {

		return fishData.size();
	}

	void updateCartEntries(@NonNull List<FishInCart> newData){
		this.fishData.clear();
		this.fishData.addAll(newData);
		this.notifyDataSetChanged();
	}

	class FishInCartViewHolder extends RecyclerView.ViewHolder {

		ImageView fishImage;
		TextView fishName;
		TextView addedDate;
		ImageView deleteImage;

		private final View.OnClickListener fishEntryClick = v -> BUS.postEvent(new FishTitleClickEvent(fishName.getText()
																											   .toString()));

		FishInCartViewHolder(View itemView) {

			super(itemView);

			fishImage = itemView.findViewById(R.id.iv_fish_cart);
			fishName = itemView.findViewById(R.id.tv_cart_fish_name);
			addedDate = itemView.findViewById(R.id.tv_cart_fish_date);
			deleteImage = itemView.findViewById(R.id.iv_cart_delete);
//
			fishName.setOnClickListener(fishEntryClick);
			fishImage.setOnClickListener(fishEntryClick);
			deleteImage.setOnClickListener(v ->
											   BUS.postEvent(new DeleteFromCartEvent(
													   fishName.getText()
															   .toString())));

		}
	}

}
