package com.alexaldev.fisher.home.settings_view;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.preference.PreferenceFragmentCompat;


import com.alexaldev.fisher.R;

public class SettingsView extends PreferenceFragmentCompat{

	public static SettingsView newInstance() {

		
		Bundle args = new Bundle();
		
		SettingsView fragment = new SettingsView();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.pref_settings);

//		getActivity().setTitle(getString(R.string.nav_settings));


	}

	@Override
	public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {

	}
}
