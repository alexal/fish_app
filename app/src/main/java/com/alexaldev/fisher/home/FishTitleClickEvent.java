package com.alexaldev.fisher.home;

/**
 * Created by alexal on 15/11/2017.
 */

public class FishTitleClickEvent {

	private final String fishName;

	public FishTitleClickEvent(String fishName) {

		this.fishName = fishName;
	}

	public String getFishName() {

		return fishName;
	}
}
