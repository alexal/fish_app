package com.alexaldev.fisher.home;

/**
 * Created by alexal on 16/11/2017.
 */

public class ViewLoadedEvent {

	private final int viewId;

	public ViewLoadedEvent(int viewId) {

		this.viewId = viewId;
	}

	public int getViewId() {

		return viewId;
	}
}
