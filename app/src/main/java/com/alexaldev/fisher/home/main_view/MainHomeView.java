package com.alexaldev.fisher.home.main_view;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alexalbasemvp.mvp.BUS;
import com.alexaldev.fisher.R;
import com.alexaldev.fisher.home.BaseHomeView;
import com.alexaldev.fisher.home.FishEntriesClickEvent;
import com.alexaldev.fisher.home.HomeController;
import com.alexaldev.fisher.home.custom_views.HomeMainFishView;

import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.OnClick;


public class MainHomeView extends BaseHomeView {

	public static final int MAX_RECENT_VIEWED_COUNT = 3; //Shows how many recent viewed items can be rendered
	public static final int MAX_RECENT_ADDED_COUNT = 3; //Shows how many recent added items can be rendered

	@BindView(R.id.iv_home_recent_not_found)
	protected ImageView recentNotFoundView;

	@BindView(R.id.recent_fish1)
	protected HomeMainFishView recentFish1;

	@BindView(R.id.recent_fish2)
	protected HomeMainFishView recentFish2;

	@BindView(R.id.recent_fish3)
	protected HomeMainFishView recentFish3;

	@BindView(R.id.iv_home_no_connection)
	protected ImageView offlineView;

	@BindView(R.id.recent_added_fish1)
	protected HomeMainFishView recentAddedFish1;

	@BindView(R.id.recent_added_fish2)
	protected HomeMainFishView recentAddedFish2;

	@BindView(R.id.recent_added_fish3)
	protected HomeMainFishView recentAddedFish3;

	@BindView(R.id.fab_home_main)
	protected FloatingActionButton fabHomeMain;

	@BindView(R.id.tv_home_recent_not_found)
	protected TextView tvHomeRecentNotFound;

	@BindView(R.id.tv_home_recent_added_not_found)
	protected TextView tvHomeRecentAddedNotFound;

	private List<HomeMainFishView> recentViewedFishViews;

	@OnClick(R.id.fab_home_main)
	protected void onClick(){
		BUS.postEvent(new FishEntriesClickEvent());
	}

	public static MainHomeView newInstance() {

		Bundle args = new Bundle();

		MainHomeView fragment = new MainHomeView();
		fragment.setArguments(args);
		return fragment;
	}


	@Override
	public Resources appResources() {

		return null;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

		super.onViewCreated(view,
							savedInstanceState);


	}

	@Override
	protected int getViewID() {

		return HomeController.MAIN_VIEW;
	}

	@Override
	public void showMessage(String message) {

		Toast.makeText(getContext(),
					   message,
					   Toast.LENGTH_SHORT)
			 .show();
	}

	@Override
	protected int getLayoutId() {

		return R.layout.home_main;
	}

	@Nullable
	@Override
	protected String getToolbarTitle() {

		return null;
	}

	@Override
	protected void initViews(View root) {

		super.initViews(root);

		this.recentViewedFishViews = new ArrayList<>();

		this.recentViewedFishViews.add(recentFish1);
		this.recentViewedFishViews.add(recentFish2);
		this.recentViewedFishViews.add(recentFish3);
	}

	void showNoRecentViewedFound() {

		this.recentNotFoundView.setVisibility(View.VISIBLE);
		this.tvHomeRecentNotFound.setVisibility(View.VISIBLE);
	}

	void showNoConnectionAvailable() {
		this.offlineView.setVisibility(View.VISIBLE);
		this.tvHomeRecentAddedNotFound.setVisibility(View.VISIBLE);
	}


	public void showRecentViewed(List<String> fishNames) {

		if (fishNames.size() > MAX_RECENT_VIEWED_COUNT) {
			throw new IllegalArgumentException("Currently cannot render more than "
													   + MAX_RECENT_VIEWED_COUNT + "recent viewed");
		}

		for (int i = 0; i < fishNames.size(); i++) {
			recentViewedFishViews.get(i)
								 .setTitle(fishNames.get(i));

			recentViewedFishViews.get(i)
								 .setVisibility(View.VISIBLE);
		}

	}

	void showRecentAdded(List<String> fishNames) {

	}



}
