package com.alexaldev.fisher.home;

import com.alexaldev.fisher.domain.dataModel.FishInCart;
import com.alexaldev.fisher.domain.repository.local.CartUpdateEvent;

import java.util.List;


interface Contract {

	interface ControllerOps {

		void openFishEntriesScreen();

		void navigateToView(int viewID);



		int getOpenViewID();

		void showRecentSearches(List<String> recentSearches);

		void showNoCartEntries();

		void showCart(List<FishInCart> data);

		void updateCart(List<FishInCart> newData);
	}

	interface PresenterOps{

		void onNavigationViewSelected(int viewID);

		/**
		 * Must annotate it with subscribe too. The event is posted on the eventbus
		 * @param loadEvent
		 */
		void onViewLoaded(ViewLoadedEvent loadEvent);

		/**
		 * Must annotate it with subscribe too. The event is posted on the eventbus
		 */
		void onFishEntriesClick(FishEntriesClickEvent event);

		/**
		 * Must annotate it with subscribe too. The event is posted on the eventbus
		 */
		void onFishClick(FishTitleClickEvent event);

		/**
		 * Must annotate it with subscribe too. The event is posted on the eventbus
		 */
		void onCartUpdate(CartUpdateEvent event);
	}
}
