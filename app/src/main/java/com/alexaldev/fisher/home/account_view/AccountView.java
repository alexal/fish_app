package com.alexaldev.fisher.home.account_view;

import android.content.res.Resources;
import android.support.annotation.Nullable;

import com.alexalbasemvp.mvp.BaseView;
import com.alexaldev.fisher.home.HomePresenter;


public class AccountView extends BaseView<HomePresenter> {

	@Override
	public Resources appResources() {

		return null;
	}

	@Override
	public void showMessage(String message) {

	}

	@Override
	protected int getLayoutId() {

		return 0;
	}

	@Nullable
	@Override
	protected String getToolbarTitle() {

		return null;
	}
}
