package com.alexaldev.fisher;

import android.app.Application;
import android.preference.PreferenceManager;
import android.util.Log;

import com.alexal.baseutils.app_util.ContextReference;
import com.alexaldev.fisher.domain.dao.FishDAO;

import io.realm.Realm;
import timber.log.Timber;

public class AppOverride extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        //Init the realm
        Realm.init(this);

        //Init the preference manager
        PreferenceManager.setDefaultValues(this,R.xml.pref_settings,false);

        //Wrap the app context so we have context access from anywhere
		ContextReference.wrap(this);

        //Init Fish data DAO, maybe there will be a better place
        FishDAO.getInstance().setup();

        if (BuildConfig.DEBUG){
        	Timber.plant(new Timber.DebugTree());
		}
		else {
        	//TODO Add fabric reporting tree on production
		}
    }

    private static class CrashReportingTree extends Timber.Tree{

		@Override
		protected void log(int priority, String tag, String message, Throwable t) {
			if (priority == Log.VERBOSE || priority == Log.DEBUG) {
				return;
			}


		}
	}
}
