package com.alexaldev.fisher.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;

import com.alexal.baseutils.app_util.ContextReference;

/**
 * Created by alexal on 7/11/2017.
 */

public class ContentUtil {

	private ContentUtil(){}

	public static Uri getResourceUri(Context context,int resourceId){
		Resources resources = context.getResources();
		return new Uri.Builder()
				.scheme(ContentResolver.SCHEME_ANDROID_RESOURCE)
				.authority(resources.getResourcePackageName(resourceId))
				.appendPath(resources.getResourceTypeName(resourceId))
				.appendPath(resources.getResourceEntryName(resourceId))
				.build();
	}

	public static Uri getResourceUri(int resourceId){
		Resources resources = ContextReference.appContext().getResources();
		return new Uri.Builder()
				.scheme(ContentResolver.SCHEME_ANDROID_RESOURCE)
				.authority(resources.getResourcePackageName(resourceId))
				.appendPath(resources.getResourceTypeName(resourceId))
				.appendPath(resources.getResourceEntryName(resourceId))
				.build();
	}


}
