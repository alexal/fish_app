package com.alexaldev.fisher.utils;

import com.alexal.baseutils.app_util.Prefs;

/**
 *  Manager for the app settings. Provides settings access set by user to anyone.
 *
 */
public class SettingsManager {

	private static final SettingsManager INSTANCE = new SettingsManager();

	public static final String FISH_LIST_DISPLAY_KEY = "pref_fish_entries_display";
	public static final int FISH_VIEW_LIST = 1;
	public static final int FISH_VIEW_GRID = 2;

	private SettingsManager(){}

	SettingsManager getInstance(){
		return INSTANCE;
	}

	public static int getFishDisplaySetting(){
		return Prefs.prefs().getInt(FISH_LIST_DISPLAY_KEY, 0);
	}

}
