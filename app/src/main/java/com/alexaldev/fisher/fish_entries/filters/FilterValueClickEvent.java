package com.alexaldev.fisher.fish_entries.filters;

/**
 * Created by alexal on 30/11/2017.
 */

public class FilterValueClickEvent {

	private final String filterValue;

	public FilterValueClickEvent(String filterValue) {

		this.filterValue = filterValue;
	}

	public String getFilterValue() {

		return filterValue;
	}
}
