package com.alexaldev.fisher.fish_entries.filters;

import com.alexaldev.fisher.domain.data_filters.FilterValue;

import java.util.HashSet;
import java.util.List;

/**
 *
 * Created by alexal on 1/12/2017.
 */

public class FiltersApplyEvent {

	private final HashSet<FilterValue> filters;

	 FiltersApplyEvent(HashSet<FilterValue> filters) {

		this.filters = new HashSet<>(filters);
	}

	public HashSet<FilterValue> getFilters() {

		return filters;
	}
}
