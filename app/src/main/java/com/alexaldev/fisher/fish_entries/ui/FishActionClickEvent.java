package com.alexaldev.fisher.fish_entries.ui;

import com.alexaldev.fisher.domain.dataModel.FishUserAction;

/**
 *
 * Created by alexal on 17/12/2017.
 */

public class FishActionClickEvent {

	private final FishUserAction fishUserAction;

	public FishActionClickEvent(FishUserAction fishUserAction) {

		this.fishUserAction = fishUserAction;
	}

	public FishUserAction getFishUserAction() {

		return fishUserAction;
	}
}
