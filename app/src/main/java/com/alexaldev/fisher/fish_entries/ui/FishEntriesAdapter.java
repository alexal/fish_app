package com.alexaldev.fisher.fish_entries.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.transition.Fade;
import android.support.transition.Transition;
import android.support.transition.TransitionManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alexalbasemvp.mvp.BUS;
import com.alexaldev.fisher.R;
import com.alexaldev.fisher.domain.dataModel.FishUserAction;
import com.alexaldev.fisher.fish_entries.domain.FishEntryViewModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CenterInside;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

import static android.support.v7.widget.RecyclerView.NO_POSITION;


public class FishEntriesAdapter
		extends RecyclerView.Adapter<FishEntriesAdapter.FishEntryViewHolder> {

	private List<FishEntryViewModel> fishData;
	private Context context;

	public FishEntriesAdapter(@NonNull Context context,
							  @NonNull List<FishEntryViewModel> data) {

		this.context = context;
		this.fishData = data;
	}

	@Override
	public FishEntryViewHolder onCreateViewHolder(ViewGroup parent,
												  int viewType) {

		final FishEntryViewHolder vh = new FishEntryViewHolder(LayoutInflater.from(parent.getContext())
																	   .inflate(R.layout.card_fish_entry_new,
																				parent,
																				false));

		vh.shareView.setOnClickListener(v -> {
			int pos = vh.getAdapterPosition();
			if (pos != NO_POSITION){
				BUS.postEvent(new FishUserAction(fishData.get(pos).getFishName(),
												 FishUserAction.ACTION_SHARE));
			}
		});

		vh.addCartView.setOnClickListener(v -> {
			int pos = vh.getAdapterPosition();
			if (pos != NO_POSITION){
				BUS.postEvent(new FishUserAction(fishData.get(pos).getFishName(),
												 FishUserAction.ACTION_ADD_CART));
			}
		});

		vh.likeView.setOnClickListener(v -> {
			int pos = vh.getAdapterPosition();
			if (pos != NO_POSITION){
				BUS.postEvent(new FishUserAction(fishData.get(pos).getFishName(),
												 FishUserAction.ACTION_LIKE));
			}
		});


		return vh;

	}

	@Override
	public void onBindViewHolder(FishEntryViewHolder holder, int position) {

		holder.tvFishEntryName.setText(
				fishData.get(position).getFishName()
		);

		holder.tvFEntrySeason.setText(
				fishData.get(position).getSeason()
		);

		holder.tvFEntryPrice.setText(
				fishData.get(position).getPrice()
		);

		holder.tvFEntryCooking.setText(
				fishData.get(position).getCooking()
		);

		holder.tvFEntryAvailability.setText(
				fishData.get(position).getAvaiability()
		);

		holder.tvFEntryKeepRange.setText(
				fishData.get(position).getKeepRange()
		);

		holder.tvFEntryPeopleEat.setText(
				fishData.get(position).getPeopleToEat()
		);

		holder.tvFEntryDescription.setText(
				fishData.get(position).getDescription()
		);

		Glide.with(context)
			 .load(fishData.get(position).getImagePath())
			 .apply(RequestOptions.placeholderOf(R.drawable.ic_fish_entries))
			 .apply(RequestOptions.bitmapTransform(new CenterCrop()))
			 .into(holder.ivFishImage);


	}


	public void updateData(List<FishEntryViewModel> newData) {

		Timber.d("Updating new data with size: %s",
				 newData.size());

		if (newData == null ||
				newData.size() == 0) {
			return;
		} else {
			this.fishData.clear();
			this.fishData.addAll(newData);

			this.notifyDataSetChanged();
		}

	}


	@Override
	public int getItemCount() {

		return fishData.size();
	}

	class FishEntryViewHolder extends RecyclerView.ViewHolder {

		@BindView(R.id.iv_f_entry_image)
		ImageView ivFishImage;

		@BindView(R.id.tv_fish_entry_name)
		TextView tvFishEntryName;

		@BindView(R.id.cl_f_entry_details_container)
		ConstraintLayout detailsContainer;

		@BindView(R.id.tv_f_entry_season)
		TextView tvFEntrySeason;

		@BindView(R.id.iv_f_entry_details)
		ImageView ivFEntryDetails;

		@BindView(R.id.tv_f_entry_price)
		TextView tvFEntryPrice;

		@BindView(R.id.tv_f_entry_cooking)
		TextView tvFEntryCooking;

		@BindView(R.id.tv_f_entry_availability)
		TextView tvFEntryAvailability;

		@BindView(R.id.tv_f_entry_keep_range)
		TextView tvFEntryKeepRange;

		@BindView(R.id.tv_f_entry_people_eat)
		TextView tvFEntryPeopleEat;

		@BindView(R.id.tv_f_entry_description)
		TextView tvFEntryDescription;

		@BindView(R.id.tv_f_entry_details)
		TextView tvEntryDetails;

		@BindView(R.id.iv_f_entry_share)
		ImageView shareView;

		@BindView(R.id.iv_f_entry_add_cart)
		ImageView addCartView;

		@BindView(R.id.iv_f_entry_like)
		ImageView likeView;

		private boolean detailsExpanded;

		FishEntryViewHolder(View itemView) {

			super(itemView);

			ButterKnife.bind(this,itemView);

			ivFEntryDetails.setOnClickListener(v -> configureDetails());
			tvEntryDetails.setOnClickListener(v -> configureDetails());

			detailsExpanded = false;
		}

		private void configureDetails(){
			if (detailsExpanded){
				TransitionManager.beginDelayedTransition(detailsContainer, new Fade());
				detailsContainer.setVisibility(View.GONE);
				ivFEntryDetails.setImageResource(R.drawable.ic_chevron_bottom);
				detailsExpanded = false;
			}
			else {
				TransitionManager.beginDelayedTransition(detailsContainer);
			   ivFEntryDetails.setImageResource(R.drawable.ic_chevron_top);
			   detailsContainer.setVisibility(View.VISIBLE);
			   detailsExpanded = true;
			}
		}

	}


}
