package com.alexaldev.fisher.fish_entries;


public class ViewLoadedEvent {

	private final int viewID;

	public ViewLoadedEvent(int viewID) {

		this.viewID = viewID;
	}

	public int getViewID() {

		return viewID;
	}
}
