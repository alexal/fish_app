package com.alexaldev.fisher.fish_entries.domain;

import com.alexaldev.fisher.domain.dataModel.Fish;

import java.util.ArrayList;
import java.util.List;

/**
 * Parses the raw fish data to the view model ones to be rendered.
 */
public class ViewModelParser {

	public List<FishEntryViewModel> parseFrom(List<Fish> rawData){

		List<FishEntryViewModel> result = new ArrayList<>();

		for (Fish f : rawData) {
			result.add(new FishEntryViewModel(f));
		}

		return result;

	}

}
