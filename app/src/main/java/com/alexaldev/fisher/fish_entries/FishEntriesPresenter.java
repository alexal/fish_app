package com.alexaldev.fisher.fish_entries;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.SearchView;

import com.alexalbasemvp.mvp.BasePresenter;
import com.alexaldev.fisher.domain.dao.FishDAO;
import com.alexaldev.fisher.domain.dataModel.Fish;
import com.alexaldev.fisher.domain.dataModel.FishUserAction;
import com.alexaldev.fisher.domain.repository.remote.RemoteStateChangedEvent;
import com.alexaldev.fisher.fish_entries.domain.ViewModelParser;
import com.alexaldev.fisher.domain.data_filters.FilterValue;
import com.alexaldev.fisher.fish_entries.filters.FiltersApplyEvent;
import com.alexaldev.fisher.fish_presentation.FPresentationPresenter;
import com.alexaldev.fisher.fish_presentation.FishPresentationController;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


import timber.log.Timber;

import static com.alexaldev.fisher.fish_entries.FishEntriesView.EMPTY_FISH_FILTERS;
import static com.alexaldev.fisher.fish_entries.FishEntriesController.FILTERS_VIEW;
import static com.alexaldev.fisher.fish_entries.FishEntriesController.FISH_ENTRIES_VIEW;

public class FishEntriesPresenter
		extends BasePresenter<FishEntriesController>
		implements Contract.PresenterOps,
				   SearchView.OnQueryTextListener{

	private ViewModelParser viewModelParser;

	//Filters cache
	private HashSet<FilterValue> appliedFilters;
	private FishDAO fishDAO;


	@Override
	protected boolean subscribesToEventBus() {

		return true;
	}

	@Override
	protected void onAttach(@NonNull FishEntriesController view) {

		this.viewModelParser = new ViewModelParser();

		this.appliedFilters = new HashSet<>();

		this.fishDAO = FishDAO.getInstance();

		getView().loadFishList();
	}


	@Override
	protected void onDetach() {
		this.fishDAO = null;
	}


	@Subscribe
	public void onEntryClick(FishEntryClickEvent event) {
		//Get the name of the fish clicked
		Intent i = new Intent(getView(),
							  FishPresentationController.class);
		i.putExtra(FPresentationPresenter.FISH_KEY,
				   event.getFishName());
		getView().startActivity(i);
	}


	@Override
	@Subscribe
	public void onViewLoaded(ViewLoadedEvent event) {

		switch (event.getViewID()) {
			case FILTERS_VIEW:
				break;
			case FISH_ENTRIES_VIEW:

				getView().showFishCount(fishDAO.getAvailableFishCount());

				getView().showFiltersCount(appliedFilters.size());

				updateViewFish(fishDAO.getFilteredFish(appliedFilters),FishEntriesView.EMPTY_FISH_FILTERS);

				this.configureRemoteState(FishDAO.REMOTE_STATE_CURRENT);

				break;
		}

	}

	private void configureRemoteState(int state){
		switch (state){
			case FishDAO.REMOTE_STATE_DISCONNECTED:
				getView().showRemoteNotConnected();
				break;
			case FishDAO.REMOTE_STATE_COMPLETED:
				getView().showRemoteConCompleted();
				getView().showFishCount(fishDAO.getAvailableFishCount());
				updateViewFish(fishDAO.getFilteredFish(appliedFilters),FishEntriesView.EMPTY_FISH_FILTERS);
				break;
			case FishDAO.REMOTE_STATE_SYNCING:
				getView().showRemoteSyncing();
				break;
		}
	}

	@Override
	public void onFilterMenuClicked() {
		getView().showFilters(new ArrayList<>(appliedFilters));
	}

	@Override
	@Subscribe
	public void onFiltersApplied(FiltersApplyEvent event) {

		Timber.d("onFiltersApplied called with size: %s",event.getFilters().size());

		this.appliedFilters.clear();
		this.appliedFilters.addAll(event.getFilters());

		getView().loadFishList();
	}

	@Override
	@Subscribe
	public void onFishActionClick(FishUserAction action) {

		Fish fish = fishDAO.getFishByName(action.getFishName());

		switch (action.getActionDescription()){

			case FishUserAction.ACTION_ADD_CART:

				if (fishDAO.fishCanBeAddedToCart(fish)){
					fishDAO.addToCart(fish);
					getView().showMessage("Προστέθηκε στο καλάθι");
				}
				else {
					getView().showMessageWithAction("Δεν μπορεί να προστεθεί",
													"Λεπτομέρειες",
													v -> getView().showMessage("Θα στα πω συντομα"));
				}

				break;
			case FishUserAction.ACTION_LIKE:
				break;
			case FishUserAction.ACTION_SHARE:
				break;
		}
	}

	@Subscribe
	public void onRemoteRepoStateChange(RemoteStateChangedEvent event){
		Timber.d("Remote state changed: %s " , event.getState());
		this.configureRemoteState(event.getState());
	}


	@Override
	public boolean onQueryTextSubmit(String query) {

		Timber.d("Text submitted: %s",query);

		return true;
	}


	private void updateViewFish(List<Fish> fishData,
								int noEntriesDisplay){
		getView().renderFishData(
				viewModelParser.parseFrom(fishData),noEntriesDisplay);
	}

	@Override
	public boolean onQueryTextChange(String newText) {

		Timber.d("Text changed: %s",newText);

		if (newText.length() <= 2){
			Timber.d("Query small, updating with current filtered");

			updateViewFish(fishDAO.getFilteredFish(appliedFilters),
						   FishEntriesView.EMPTY_FISH_FILTERS);

		}

		if (newText.length() > 2){
			Timber.d("Query size good, updating with fish search by prefix");
			updateViewFish(fishDAO.getFishByPrefix(newText),FishEntriesView.EMPTY_FISH_SEARCH);
		}

		return true;
	}
}
