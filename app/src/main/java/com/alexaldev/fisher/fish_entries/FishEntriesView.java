package com.alexaldev.fisher.fish_entries;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alexalbasemvp.mvp.BUS;
import com.alexalbasemvp.mvp.BaseView;
import com.alexaldev.fisher.R;
import com.alexaldev.fisher.fish_entries.domain.FishEntryViewModel;
import com.alexaldev.fisher.fish_entries.ui.FishEntriesAdapter;
import com.pchmn.materialchips.ChipView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

/**
 *
 */
public class FishEntriesView extends BaseView<FishEntriesPresenter> {

	//Used for the case that a requested render for fish data contains empty list
	public static final int EMPTY_FISH_FILTERS = 666;
	public static final int EMPTY_FISH_SEARCH = 667;

	public static final int REMOTE_OFFLINE = 123;
	public static final int REMOTE_SYNCING = 124;
	public static final int REMOTE_ONLINE = 125;

	@BindView(R.id.iv_entries_warning_icon)
	protected ImageView ivEntriesWarningIcon;

	@BindView(R.id.tv_entries_not_found)
	protected TextView tvEntriesNotFound;

	@BindView(R.id.rv_fish_list)
	protected RecyclerView fishListView;

	@BindView(R.id.iv_fish_entries_remote_state)
	protected ImageView ivFishEntriesRemoteState;

	@BindView(R.id.progress_remote_syncing)
	protected ProgressBar progressRemoteSyncing;

	@BindView(R.id.chipView_fish_available)
	protected ChipView chipViewFishAvailable;

	@BindView(R.id.chipVIew_filters_count)
	protected ChipView chipVIewFiltersCount;

	private FishEntriesAdapter mAdapter;


	public static FishEntriesView newInstance() {


		Bundle args = new Bundle();

		FishEntriesView fragment = new FishEntriesView();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	protected void initViews(View root) {

		super.initViews(root);

		fishListView.setLayoutManager(
				new LinearLayoutManager(getContext(),
										LinearLayoutManager.VERTICAL,
										false));

		if (mAdapter == null) {

			mAdapter = new FishEntriesAdapter(getContext(),
											  new ArrayList<>());
		}

		fishListView.setAdapter(mAdapter);

		BUS.postEvent(new ViewLoadedEvent(
				FishEntriesController.FISH_ENTRIES_VIEW
		));

	}

	@Override
	protected int getCustomHomeIndicator() {

		return R.drawable.ic_chevron_left;
	}

	@Override
	protected int getLayoutId() {

		return R.layout.layout_fish_entries_list;
	}

	@Nullable
	@Override
	protected String getToolbarTitle() {

		return "Ψαραγορά";
	}

	public void updateFishCount(int count){
		this.chipViewFishAvailable.setLabel(count + " διαθέσιμα");
	}

	public void updateFiltersCount(int count){
		if (count == 0)
			this.chipVIewFiltersCount.setVisibility(View.GONE);
		else {
			this.chipVIewFiltersCount.setVisibility(View.VISIBLE);
			this.chipVIewFiltersCount.setLabel(count + " φίλτρα");
		}

	}

	/**
	 * Updates the remote state view. Must be one of the provided static ids for the remote state
	 * @param remoteState
	 */
	public void updateRemoteState(int remoteState){

		switch (remoteState){
			case REMOTE_OFFLINE:
				this.progressRemoteSyncing.setVisibility(View.GONE);
				this.ivFishEntriesRemoteState.setVisibility(View.VISIBLE);
				this.ivFishEntriesRemoteState.setImageResource(R.drawable.ic_cloud_off);
				break;
			case REMOTE_ONLINE:
				this.progressRemoteSyncing.setVisibility(View.GONE);
				this.ivFishEntriesRemoteState.setVisibility(View.VISIBLE);
				this.ivFishEntriesRemoteState.setImageResource(R.drawable.ic_cloud_done);
				break;
			case REMOTE_SYNCING:
				this.progressRemoteSyncing.setVisibility(View.VISIBLE);
				this.ivFishEntriesRemoteState.setVisibility(View.GONE);
				break;
			default:
				Timber.d("Unknown remote state view provided.");
		}
	}

	public void updateFishData(@NonNull List<FishEntryViewModel> newData) {

		this.tvEntriesNotFound.setVisibility(View.GONE);
		this.ivEntriesWarningIcon.setVisibility(View.GONE);
		this.fishListView.setVisibility(View.VISIBLE);

		this.mAdapter.updateData(newData);

	}

	public void showNotFoundEntries(int reason) {

		this.tvEntriesNotFound.setVisibility(View.VISIBLE);
		this.ivEntriesWarningIcon.setVisibility(View.VISIBLE);

		this.fishListView.setVisibility(View.GONE);

		switch (reason) {
			case EMPTY_FISH_FILTERS:
				tvEntriesNotFound.setText(getString(R.string.entries_filters_no_entries));
				break;
			case EMPTY_FISH_SEARCH:
				tvEntriesNotFound.setText(getString(R.string.entries_search_no_entries));
				break;
		}
	}

}

