package com.alexaldev.fisher.fish_entries;

import android.app.FragmentTransaction;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.alexalbasemvp.mvp.BaseController;
import com.alexaldev.fisher.R;
import com.alexaldev.fisher.domain.data_filters.FilterValue;
import com.alexaldev.fisher.fish_entries.domain.FishEntryViewModel;
import com.alexaldev.fisher.fish_entries.filters.FiltersView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import timber.log.Timber;


public class FishEntriesController
		extends BaseController<FishEntriesController, FishEntriesPresenter>
		implements Contract.ControllerOps {

	@BindView(R.id.toolbar)
	protected Toolbar toolbar;

	@BindView(R.id.fl_fish_list)
	protected FrameLayout flFishList;

	@BindView(R.id.cl_entries_root)
	protected CoordinatorLayout root;

	//Components
	private FishEntriesView fishEntriesView;
	private FiltersView filtersView;

	public static final int FISH_ENTRIES_VIEW = 1420;
	public static final int FILTERS_VIEW = 1421;

	@IdRes
	private int FRAGMENT_CONTAINER = R.id.fl_fish_list;

	@Override
	protected int getLayoutId() {

		return R.layout.layout_fish_entries;
	}

	@Nullable
	@Override
	protected Toolbar getToolbar() {

		return toolbar;
	}

	@Override
	public void onBackPressed() {

		if (filtersView != null &&
				filtersView.isVisible()){
			loadFishList();
		}
		else {
			super.onBackPressed();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		Timber.d("Creating Options menu..");

		getMenuInflater().inflate(R.menu.menu_fish_entries,
								  menu);

		//Configure the search icon
		MenuItem searchItem = menu.findItem(R.id.menu_f_entries_search);
		SearchView searchView = (SearchView) searchItem.getActionView();
		searchView.setOnQueryTextListener(getPresenter());

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
			case R.id.menu_f_entries_filter:
				getPresenter().onFilterMenuClicked();
		}

		return true;
	}

	@Nullable
	@Override
	protected String getToolbarTitle() {

		return "ψαραγορα";
	}


	@Override
	protected boolean homeAsUpEnabled() {

		return true;
	}


	@Override
	protected void initViews() {

	}

	@Override
	public void showMessage(String message) {

		Snackbar.make(flFishList,message,Snackbar.LENGTH_SHORT).show();
	}

	@Override
	protected FishEntriesController getThis() {

		return this;
	}

	@Override
	protected FishEntriesPresenter newPresenter() {

		return new FishEntriesPresenter();
	}

	@Override
	protected boolean preservePresenter() {

		return true;

	}

	@Override
	public void showMessageWithAction(String message, String actionMessage,
									  View.OnClickListener actionListener) {

		Snackbar snackbar = Snackbar.make(root,
										  message,
										  Snackbar.LENGTH_LONG);

		snackbar.setAction(actionMessage,actionListener);

		snackbar.setActionTextColor(ContextCompat.getColor(this,R.color.primary));

		snackbar.show();
	}

	@Override
	public void loadFishList() {

		if (this.fishEntriesView == null){
			this.fishEntriesView = FishEntriesView.newInstance();
		}

//		fishEntriesView = FishEntriesView.newInstance();

		replaceTransaction(fishEntriesView,
						   FragmentTransaction.TRANSIT_FRAGMENT_FADE);
	}


	@Override
	public void showFishCount(int count) {
		fishEntriesView.updateFishCount(count);
	}

	@Override
	public void showRemoteConCompleted() {
		this.fishEntriesView.updateRemoteState(FishEntriesView.REMOTE_ONLINE);
	}

	@Override
	public void showRemoteSyncing() {
		this.fishEntriesView.updateRemoteState(FishEntriesView.REMOTE_SYNCING);
	}

	@Override
	public void showRemoteNotConnected() {
		this.fishEntriesView.updateRemoteState(FishEntriesView.REMOTE_OFFLINE);
	}


	@Override
	protected int getFragmentContainer() {

		return FRAGMENT_CONTAINER;
	}

	@Override
	public void renderFishData(List<FishEntryViewModel> fishData,int emptyListDisplay) {

		if (fishData.size() == 0){
			this.fishEntriesView.showNotFoundEntries(emptyListDisplay);
		}
		else {
			this.fishEntriesView.updateFishData(
					fishData);
		}


	}

	@Override
	public void showFiltersCount(int filtersCount) {
		this.fishEntriesView.updateFiltersCount(filtersCount);
	}

	@Override
	public void showFilters(ArrayList<FilterValue> selectedFilters) {

		filtersView = FiltersView.newInstance(selectedFilters);
		filtersView.setPresenter(getPresenter());

		replaceTransaction(filtersView,
						   FragmentTransaction.TRANSIT_FRAGMENT_FADE);

	}

}
