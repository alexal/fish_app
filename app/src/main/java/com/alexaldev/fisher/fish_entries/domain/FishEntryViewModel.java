package com.alexaldev.fisher.fish_entries.domain;

import com.alexaldev.fisher.domain.dataModel.Fish;

/**
 * The model rendered as the fish entry.
 */
public class FishEntryViewModel {

	private final String imagePath;
	private final String fishName;
	private final int likesCount;
	private final String season;
	private final String price;
	private final String cooking;
	private final String avaiability;
	private final String keepRange;
	private final String description;
	private final String peopleToEat;


	public FishEntryViewModel(String imagePath, String fishName, int likesCount,
							  String season, String price, String cooking, String avaiability,
							  String keepRange, String description, String peopleToEat) {

		this.imagePath = imagePath;
		this.fishName = fishName;
		this.likesCount = likesCount;
		this.season = season;
		this.price = price;
		this.cooking = cooking;
		this.avaiability = avaiability;
		this.keepRange = keepRange;
		this.description = description;
		this.peopleToEat = peopleToEat;
	}

	public FishEntryViewModel(Fish f){
		this.imagePath = f.getImageUrl();
		this.fishName = f.getName();
		this.likesCount = f.getLikesCount();
		this.season = f.getSeason();
		this.price = f.getPrice();
		this.cooking = f.getCooking();
		this.avaiability = f.getAvailability();
		this.keepRange = f.getKeepRange();
		this.description = f.getDescription();
		this.peopleToEat = f.getPeopleCanEat();
	}

	public String getFishName() {

		return fishName;
	}

	public int getLikesCount() {

		return likesCount;
	}

	public String getImagePath() {

		return imagePath;
	}

	public String getSeason() {

		return season;
	}

	public String getPrice() {

		return price;
	}

	public String getCooking() {

		return cooking;
	}

	public String getAvaiability() {

		return avaiability;
	}

	public String getKeepRange() {

		return keepRange;
	}

	public String getDescription() {

		return description;
	}

	public String getPeopleToEat() {

		return peopleToEat;
	}
}
