package com.alexaldev.fisher.fish_entries.filters;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alexalbasemvp.mvp.BUS;
import com.alexaldev.fisher.R;

/**
 *
 * Created by alexal on 30/11/2017.
 */

public class FilterValueView extends LinearLayout{

	TextView filterTitleView;
	ImageView filterIconView;

	public FilterValueView(Context context, AttributeSet attrs) {

		super(context,
			  attrs);

		LinearLayout root = (LinearLayout) LayoutInflater.from(context)
														 .inflate(R.layout.view_filter_value,this,true);

		filterTitleView = root.findViewById(R.id.tv_filter_value_title);
		filterIconView = root.findViewById(R.id.iv_filter_value_icon);

		TypedArray a = context.obtainStyledAttributes(attrs,
													  R.styleable.FilterValueView,0,0);

		String filterTitle = a.getString(R.styleable.FilterValueView_title);

		filterTitleView.setText(filterTitle);

		filterIconView.setImageDrawable(a.getDrawable(R.styleable.FilterValueView_icon));

		a.recycle();

		OnClickListener listener = v ->  BUS.postEvent(new FilterValueClickEvent(filterTitleView.getText().toString()));

		filterTitleView.setOnClickListener(listener);
		filterIconView.setOnClickListener(listener);
	}


}
