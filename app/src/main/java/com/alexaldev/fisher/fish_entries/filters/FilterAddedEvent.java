package com.alexaldev.fisher.fish_entries.filters;

import com.alexaldev.fisher.domain.data_filters.FilterValue;

/**
 *
 * Created by alexal on 1/12/2017.
 */

public class FilterAddedEvent {

	private final FilterValue filter;

	public FilterAddedEvent(FilterValue filter) {

		this.filter = new FilterValue(filter.getAttributeName(),
									  filter.getAttributeValue());
	}

	public FilterValue getFilter() {

		return filter;
	}
}
