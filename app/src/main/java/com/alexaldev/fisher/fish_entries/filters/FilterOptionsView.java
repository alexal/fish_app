package com.alexaldev.fisher.fish_entries.filters;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alexalbasemvp.mvp.BUS;
import com.alexaldev.fisher.R;
import com.alexaldev.fisher.domain.dataModel.FishAttrs;
import com.alexaldev.fisher.domain.data_filters.FilterValue;

import org.greenrobot.eventbus.Subscribe;


/**
 *
 * Created by alexal on 29/11/2017.
 */
public class FilterOptionsView extends BottomSheetDialogFragment {

	public static final String OPTIONS_KEY = "options_key";
	public static final int FILTERS_CATEGORY = 421;
	public static final int FILTERS_SEASON = 422;

	private int filterID;

	public static FilterOptionsView newInstance(int filterID) {


		Bundle args = new Bundle();
		args.putInt(OPTIONS_KEY,
					filterID);

		FilterOptionsView fragment = new FilterOptionsView();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		filterID = getArguments().getInt(OPTIONS_KEY);

		BUS.subscribe(this);
	}

	@Override
	public void onDestroy() {
		BUS.unsubscribe(this);

		super.onDestroy();
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
							 @Nullable Bundle savedInstanceState) {

		switch (filterID) {
			case FILTERS_CATEGORY:
				return inflater.inflate(R.layout.view_filter_category,
										container,
										false);
			case FILTERS_SEASON:
				return inflater.inflate(R.layout.view_filter_season,
										container,
										false);
			default:
				return null;

		}

	}

	private String parseFilterToAttribute(int filterID){
		switch (filterID) {
			case FILTERS_CATEGORY:
				return FishAttrs.Category.KEY;
			case FILTERS_SEASON:
				return FishAttrs.Season.KEY;
			default:
				return null;
		}
	}

	@Subscribe
	protected void onValueSelected(FilterValueClickEvent event){
			BUS.postEvent(new FilterAddedEvent(
					new FilterValue(parseFilterToAttribute(filterID), event.getFilterValue())
			));
			this.dismiss();
	}




}
