package com.alexaldev.fisher.fish_entries;

import android.support.annotation.Nullable;

import com.alexaldev.fisher.domain.dataModel.FishUserAction;
import com.alexaldev.fisher.domain.data_filters.FilterValue;
import com.alexaldev.fisher.fish_entries.domain.FishEntryViewModel;
import com.alexaldev.fisher.fish_entries.filters.FiltersApplyEvent;

import java.util.ArrayList;
import java.util.List;

interface Contract {

	interface ControllerOps {

		void loadFishList();

		void showFishCount(int count);

		void showRemoteConCompleted();

		void showRemoteSyncing();

		void showRemoteNotConnected();

		/**
		 * Renders the given fish data, on the given list id. Currently
		 * the lists are summer,winter and all year seasons, so give them one of those.
		 * @param fishData
		 */
		void renderFishData(List<FishEntryViewModel> fishData,
							int emptyListDisplay);

		void showFiltersCount(int filtersCount);

		void showFilters(@Nullable  ArrayList<FilterValue> selectedFilters);

	}

	interface PresenterOps{

		/**
		 * Callback fired when the fish list has been loaded on the view container.
		 * It is safe to load the fish data from here.
		 * WARNING: Must annotate with subscribe, this event is delivered through bus.
		 */
		void onViewLoaded(ViewLoadedEvent event);

		void onFilterMenuClicked();

		void onFiltersApplied(FiltersApplyEvent event);

		/**
		 * Fired from the fish adapter when a user clicks on any action that can be done on the fish.
		 * WARNING: This event is fired through BUS so, you need to annotate it with subscribe.
		 * @param action
		 */
		void onFishActionClick(FishUserAction action);

	}
}
