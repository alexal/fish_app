package com.alexaldev.fisher.fish_entries.filters;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.alexalbasemvp.mvp.BUS;
import com.alexalbasemvp.mvp.BaseView;
import com.alexaldev.fisher.R;
import com.alexaldev.fisher.domain.dataModel.FishAttrs;
import com.alexaldev.fisher.domain.data_filters.FilterValue;
import com.alexaldev.fisher.fish_entries.FishEntriesPresenter;
import org.greenrobot.eventbus.Subscribe;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;


public class FiltersView extends BaseView<FishEntriesPresenter> {

	public static final String INITIAL_FILTERS_KEY = "initialFiltersKey";

	@BindView(R.id.tv_filters_reset)
	protected TextView tvFiltersReset;

	@BindView(R.id.tv_filters_apply)
	protected TextView tvFiltersApply;

	@BindView(R.id.tv_filter_category)
	protected TextView tvFilterCategory;

	@BindView(R.id.tv_filter_category_selected)
	protected TextView tvFilterCategorySelected;

	@BindView(R.id.tv_filter_season)
	protected TextView tvFilterSeason;

	@BindView(R.id.tv_filter_season_selected)
	protected TextView tvFilterSeasonSelected;

	@BindView(R.id.iv_filter_category)
	protected ImageView ivFilterCategory;

	@BindView(R.id.iv_filter_season)
	protected ImageView ivFilterSeason;

	private HashSet<FilterValue> filterValues; //The selected filters cached until the user exits the view

	public static FiltersView newInstance(@Nullable  ArrayList<FilterValue> selectedFilters) {

		Bundle args = new Bundle();
		args.putParcelableArrayList(INITIAL_FILTERS_KEY,selectedFilters);

		FiltersView fragment = new FiltersView();
		fragment.setArguments(args);
		return fragment;
	}

	public static FiltersView newInstance() {


		Bundle args = new Bundle();

		FiltersView fragment = new FiltersView();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		this.filterValues = new HashSet<>();

		if (getArguments() != null){

			ArrayList<FilterValue> initialFilters = getArguments().getParcelableArrayList(INITIAL_FILTERS_KEY);

			if (initialFilters != null){

				Timber.d("Found initial filters state with size: %s",initialFilters.size());
				filterValues.addAll(initialFilters);
			}
		}

		setHasOptionsMenu(true);

		BUS.subscribe(this);

	}


	@Override
	protected int getCustomHomeIndicator() {

		return R.drawable.ic_close;
	}

	@Override
	public void onDestroy() {

		BUS.unsubscribe(this);

		super.onDestroy();
	}

	@Override
	protected int getLayoutId() {

		return R.layout.filters_view;
	}


	@Override
	protected void initViews(View root) {

		super.initViews(root);

		for (FilterValue filterValue : filterValues){
			this.renderFilterValue(filterValue);
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

		super.onCreateOptionsMenu(menu,
								  inflater);
		menu.clear();
	}

	@Nullable
	@Override
	protected String getToolbarTitle() {

		return null;
	}

	@OnClick({ R.id.iv_filter_category, R.id.tv_filter_category})
	protected void onCategoryClick(){

		getActivity().getSupportFragmentManager()
					 .beginTransaction()
					 .add(FilterOptionsView.newInstance(
					 		FilterOptionsView.FILTERS_CATEGORY
					 ),"CATEGORY")
					 .commit();
	}

	@OnClick({R.id.iv_filter_season, R.id.tv_filter_season})
	protected void onSeasonClick(){

		getActivity().getSupportFragmentManager()
					 .beginTransaction()
					 .add(FilterOptionsView.newInstance(
							 FilterOptionsView.FILTERS_SEASON
					 ),"CATEGORY")
					 .commit();
	}

	private void renderCurrentFilters(){
		for (FilterValue f : this.filterValues)
			renderFilterValue(f);
	}

	private void renderFilterValue(@NonNull FilterValue filterValue){

		switch (filterValue.getAttributeName()){

			case FishAttrs.Category.KEY:
				tvFilterCategorySelected.setVisibility(View.VISIBLE);
				tvFilterCategorySelected.setText(filterValue.getAttributeValue());
				break;

			case FishAttrs.Season.KEY:
				tvFilterSeasonSelected.setVisibility(View.VISIBLE);
				tvFilterSeasonSelected.setText(filterValue.getAttributeValue());
		}

	}

	@Subscribe
	protected void onFilterAddedEvent(FilterAddedEvent event){

		Timber.d("Filter added");

		//Yes this is really stupid

		if (this.filterValues.contains(event.getFilter())){
			this.filterValues.remove(event.getFilter());
			this.filterValues.add(event.getFilter());
			Timber.d("Filter was replaced in cache");
		}else {
			this.filterValues.add(event.getFilter());
		}

		this.renderFilterValue(event.getFilter());
	}

	@OnClick(R.id.tv_filters_apply)
	protected void onApplyFiltersClick(){

		BUS.postEvent(new FiltersApplyEvent(filterValues));
	}

	@OnClick(R.id.tv_filters_reset)
	protected void onResetClick(){
		filterValues.clear();
		this.resetSelections();
	}

	private void resetSelections(){
		tvFilterSeasonSelected.setVisibility(View.GONE);
		tvFilterCategorySelected.setVisibility(View.GONE);
	}


}
