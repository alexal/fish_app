package com.alexaldev.fisher.fish_entries;

/**
 * Created by alexal on 3/11/2017.
 */

public class FishEntryClickEvent {

	private final String fishName;

	public FishEntryClickEvent(String fishName) {

		this.fishName = fishName;
	}

	public String getFishName() {

		return fishName;
	}
}
