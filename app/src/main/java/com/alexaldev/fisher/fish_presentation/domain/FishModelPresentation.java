package com.alexaldev.fisher.fish_presentation.domain;

public class FishModelPresentation {

	private String fishName;

	private String imageUri;

	private String description;

	private String cooking;

	private String price;

	private String availability;

	private String peopleCount;

	private String keepRange;

	private String regularWeight;

	private String season;

	private boolean canBeAddedInCart;

	private boolean canBeLiked;

	private FishModelPresentation(String fishName, String imageUri, String description,
								  String cooking, String price, String availability, String peopleCount,
								  String keepRange, String regularWeight, String season, boolean canBeLiked,
								  boolean canBeAddedInCart) {

		this.fishName = fishName;
		this.imageUri = imageUri;
		this.description = description;
		this.cooking = cooking;
		this.price = price;
		this.availability = availability;
		this.peopleCount = peopleCount;
		this.keepRange = keepRange;
		this.regularWeight = regularWeight;
		this.season = season;
		this.canBeAddedInCart = canBeAddedInCart;
		this.canBeLiked = canBeLiked;
	}

	public boolean canBeAddedInCart() {

		return canBeAddedInCart;
	}

	public boolean canBeLiked() {

		return canBeLiked;
	}

	public String getFishName() {

		return fishName;
	}

	public String getImageUri() {

		return imageUri;
	}

	public String getDescription() {

		return description;
	}

	public String getCooking() {

		return cooking;
	}

	public String getPrice() {

		return price;
	}

	public String getAvailability() {

		return availability;
	}

	public String getPeopleCount() {

		return peopleCount;
	}

	public String getKeepRange() {

		return keepRange;
	}

	public String getRegularWeight() {

		return regularWeight;
	}

	public String getSeason() {

		return season;
	}

	static Builder newBuilder(){
		return new Builder();
	}

	static class Builder {

		private String fishName;
		private String imageUri;
		private String description;
		private String cooking;
		private String price;
		private String availability;
		private String peopleCount;
		private String keepRange;
		private String regularWeight;
		private String season;
		private boolean canBeAddedInCart;
		private boolean canBeLiked;


		public Builder setFishName(String fishName) {

			this.fishName = fishName;
			return this;
		}

		public Builder setImageUri(String imageUri) {

			this.imageUri = imageUri;
			return this;
		}


		public Builder setDescription(String description) {

			this.description = description;
			return this;
		}

		public Builder canBeAddedInCart(boolean canBeAddedInCart){
			this.canBeAddedInCart = canBeAddedInCart;
			return this;
		}

		public Builder canBeLiked(boolean canBeLiked){
			this.canBeLiked = canBeLiked;
			return this;
		}

		public Builder setCooking(String cooking) {

			this.cooking = cooking;
			return this;
		}

		public Builder setPrice(String price) {

			this.price = price;
			return this;
		}

		public Builder setAvailability(String availability) {

			this.availability = availability;
			return this;
		}

		public Builder setPeopleCount(String peopleCount) {

			this.peopleCount = peopleCount;
			return this;
		}

		public Builder setKeepRange(String keepRange) {

			this.keepRange = keepRange;
			return this;
		}

		public Builder setRegularWeight(String regularWeight) {

			this.regularWeight = regularWeight;
			return this;
		}

		public Builder setSeason(String season) {

			this.season = season;
			return this;
		}

		public FishModelPresentation createFishModel() {

			return new FishModelPresentation(fishName,
											 imageUri,
											 description,
											 cooking,
											 price,
											 availability,
											 peopleCount,
											 keepRange,
											 regularWeight,
											 season,
											 canBeLiked,
											 canBeAddedInCart);
		}
	}
}
