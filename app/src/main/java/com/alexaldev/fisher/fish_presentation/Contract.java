package com.alexaldev.fisher.fish_presentation;

import com.alexaldev.fisher.domain.dataModel.FishUserAction;
import com.alexaldev.fisher.fish_presentation.domain.FishModelPresentation;

/**
 * Created by alexal on 5/11/2017.
 */

public interface Contract {

	interface ViewOps{

		void renderFishInfo(FishModelPresentation fish);

		void showAddedToCartMessage();
	}

	interface PresenterOps{

		void onAddToCartClick();

		void onLikeClick();


	}
}
