package com.alexaldev.fisher.fish_presentation;


/**
 * Represents a fish view event.
 */
public class FishViewedEvent {

	private final String fishName;

	public FishViewedEvent(String fishName) {

		this.fishName = fishName;
	}

	public String getFishName() {

		return fishName;
	}
}
