package com.alexaldev.fisher.fish_presentation;

import android.content.Intent;
import android.support.annotation.NonNull;

import com.alexalbasemvp.mvp.BasePresenter;
import com.alexalbasemvp.mvp.BUS;
import com.alexaldev.fisher.R;
import com.alexaldev.fisher.domain.dataModel.Fish;
import com.alexaldev.fisher.domain.dao.FishDAO;
import com.alexaldev.fisher.fish_presentation.domain.FishModelParser;
import com.alexaldev.fisher.home.cart_view.DeleteFromCartEvent;


public class FPresentationPresenter extends BasePresenter<FishPresentationController>
			implements Contract.PresenterOps{

	public static final String FISH_KEY = "FISH_KEY";

	private Fish fish;


	@Override
	protected boolean subscribesToEventBus() {

		return false;
	}

	@Override
	protected void onAttach(@NonNull FishPresentationController view) {
		//Handle the creation process from onComingIntent
	}

	@Override
	protected void onDetach() {

	}

	@Override
	protected void onComingIntent(Intent intent) {

		super.onComingIntent(intent);

		//The screen shows up with a coming intent containing the fish name to present

		//Get it
		String fishName = intent.getStringExtra(FISH_KEY);

		fish = FishDAO.getInstance()
				.getFishByName(fishName);

		getView().renderFishInfo(
				new FishModelParser()
						.parseFrom(fish)
		);

		//Send a fish view event to someone interested, currently FishDAO listens the event
		BUS.postEvent(new FishViewedEvent(fishName));
		
	}

	@Override
	public void onAddToCartClick() {

		if (!FishDAO.getInstance().fishInCart(fish)){
			if (FishDAO.getInstance().fishCanBeAddedToCart(fish)){

				FishDAO.getInstance().addToCart(fish);
				getView().showAddedToCartMessage();
			}
			else {
				getView().showMessage(appResources().getString(R.string.fp_fish_not_able_cart));
			}

		}
		else {
			getView().showMessageWithAction(appResources().getString(R.string.fp_fish_in_cart),
											appResources().getString(R.string.fp_fish_remove_cart),
											v -> BUS.postEvent(new DeleteFromCartEvent(fish.getName())));
		}



	}

	@Override
	public void onLikeClick() {
		getView().showMessage("Η λειτουργία δεν είναι ακόμα διαθέσιμη :( ");
	}
}
