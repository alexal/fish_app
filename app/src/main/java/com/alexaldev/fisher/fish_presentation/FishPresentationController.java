package com.alexaldev.fisher.fish_presentation;


import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alexalbasemvp.mvp.BaseController;
import com.alexaldev.fisher.R;
import com.alexaldev.fisher.fish_presentation.domain.FishModelPresentation;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.OnClick;


public class FishPresentationController extends BaseController<FishPresentationController, FPresentationPresenter>
		implements Contract.ViewOps {

	@BindView(R.id.layout_root_f_presentation)
	protected LinearLayout root;

	@BindView(R.id.toolbar)
	protected Toolbar toolbar;

	@BindView(R.id.toolbarMenu)
	protected Toolbar toolbarMenu;

	@BindView(R.id.iv_fp_image)
	protected ImageView fishImage;

	@BindView(R.id.tv_fp_description)
	protected TextView tvFpDescription;

	@BindView(R.id.cardview_fish_description)
	protected CardView cardviewFishDescription;

	@BindView(R.id.iv_fp_desrc_chevron)
	protected ImageView chevronIcon;

	@BindView(R.id.tv_fp_price)
	protected TextView tvFpPrice;

	@BindView(R.id.tv_fp_cooking)
	protected TextView tvFpCooking;

	@BindView(R.id.tv_fp_availability)
	protected TextView tvFpAvailability;

	@BindView(R.id.tv_fp_people)
	protected TextView tvFpPeople;

	@BindView(R.id.tv_fp_keep_range)
	protected TextView tvFpKeepRange;

	@BindView(R.id.tv_fp_season)
	protected TextView tvFpSeason;

	private static final int CHEVRON_RIGHT = R.drawable.ic_chevron_right;
	private static final int CHEVRON_BOTTOM = R.drawable.ic_chevron_bottom;

	@OnClick(R.id.cardview_fish_description)
	protected void onDescriptionCardClick() {

		if (tvFpDescription.getVisibility() == View.VISIBLE) {
			tvFpDescription.setVisibility(View.GONE);
			chevronIcon.setImageResource(CHEVRON_RIGHT);
		} else {
			tvFpDescription.setVisibility(View.VISIBLE);
			chevronIcon.setImageResource(CHEVRON_BOTTOM);

		}

	}

	@Override
	protected void initViews() {

		toolbarMenu.inflateMenu(R.menu.menu_fish_presentation);

		toolbarMenu.setOnMenuItemClickListener(this::onOptionsItemSelected);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
			case R.id.menu_f_presentation_add_cart:
				getPresenter().onAddToCartClick();
				break;
			case R.id.menu_f_presentation_like:
				getPresenter().onLikeClick();
				break;
		}

		return true;
	}

	@Override
	public void onBackPressed() {

		this.finish();
	}

	@Override
	protected FishPresentationController getThis() {

		return this;
	}

	@Override
	protected FPresentationPresenter newPresenter() {

		return new FPresentationPresenter();
	}

	@Override
	protected int getLayoutId() {

		return R.layout.layout_fish_presentation;
	}

	@Nullable
	@Override
	protected Toolbar getToolbar() {

		return toolbar;
	}

	@Override
	protected String getToolbarTitle() {

		return "";
	}

	@Override
	protected boolean homeAsUpEnabled() {

		return true;
	}

	@Override
	protected boolean preservePresenter() {

		return true;
	}

	@Override
	public void renderFishInfo(FishModelPresentation fish) {

		toolbarMenu.setTitle(fish.getFishName());

		Glide.with(this)
			 .load(fish.getImageUri())
			 .into(fishImage);

		tvFpDescription.setText(fish.getDescription());
		tvFpPrice.setText(fish.getPrice());
		tvFpAvailability.setText(fish.getAvailability());
		tvFpCooking.setText(fish.getCooking());
		tvFpKeepRange.setText(fish.getKeepRange());
		tvFpPeople.setText(fish.getPeopleCount());
		tvFpSeason.setText(fish.getSeason());
	}

	@Override
	public void showMessage(String message) {

		Snackbar.make(root,
					  message,
					  Snackbar.LENGTH_LONG);

	}

	public void showMessageWithAction(String message,
									  String action,
									  View.OnClickListener actionListener){

		Snackbar snackbar = Snackbar.make(root,
										  message,
										  Snackbar.LENGTH_LONG);

		snackbar.setAction(action,actionListener);
		snackbar.setActionTextColor(ContextCompat.getColor(this,R.color.primary));

		snackbar.show();

	}

	@Override
	public void showAddedToCartMessage() {

		Snackbar snackbar = Snackbar.make(root,
										  getString(R.string.fp_fish_added_cart),
										  Snackbar.LENGTH_SHORT);
//		snackbar.setAction(getString(R.string.home_cart),
//						   v -> {
//					Intent homeIntent = new Intent(this,HomeController.class);
//					homeIntent.putExtra(HomeController.OPEN_SPECIFIC_VIEW,HomeController.CART_VIEW);
//
//							   startActivity(homeIntent);
//						   });
//
//		snackbar.setActionTextColor(ContextCompat.getColor(this,R.color.primary));
		snackbar.show();


	}

}
