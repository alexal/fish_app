package com.alexaldev.fisher.fish_presentation.domain;

import android.support.annotation.NonNull;

import com.alexaldev.fisher.domain.dataModel.Fish;

/**
 * Created by alexal on 5/11/2017.
 */

public class FishModelParser {

 public FishModelPresentation parseFrom(@NonNull Fish fish){

		FishModelPresentation.Builder builder = FishModelPresentation.newBuilder();

		builder.setDescription(fish.getDescription())
			   .setAvailability(fish.getAvailability())
			   .setCooking(fish.getCooking())
			   .setFishName(fish.getName())
			   .setKeepRange(fish.getKeepRange())
			   .setImageUri(fish.getImageUrl())
			   .setPeopleCount(fish.getPeopleCanEat())
			   .setPrice(fish.getPrice())
			   .setSeason(fish.getSeason());



		return builder.createFishModel();

	}

}
