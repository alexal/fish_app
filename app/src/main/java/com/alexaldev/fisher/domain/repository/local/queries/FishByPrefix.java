package com.alexaldev.fisher.domain.repository.local.queries;

import com.alexaldev.fisher.domain.dataModel.Fish;
import com.alexaldev.fisher.domain.dataModel.GreekCapitalize;
import com.alexaldev.fisher.domain.repository.local.RealmSpecification;

import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmResults;
import timber.log.Timber;

/**
 *
 * Created by alexal on 12/12/2017.
 */

public class FishByPrefix implements RealmSpecification<Fish>{

	private static final FishByPrefix INSTANCE = new FishByPrefix();
	private String prefix;

	public static FishByPrefix withPrefix(String prefix){
		INSTANCE.prefix = GreekCapitalize.capitalizeWithoutTone(prefix.toCharArray());
		return INSTANCE;
	}

	@Override
	public RealmResults<Fish> toRealmResults(Realm realm) {

		Timber.d("Searching for: %s",prefix);

		return realm.where(Fish.class)
					.beginsWith("searchableName",prefix)
				.findAll();
	}


}
