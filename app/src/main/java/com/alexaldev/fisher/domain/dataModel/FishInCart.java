package com.alexaldev.fisher.domain.dataModel;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class FishInCart extends RealmObject {

	@PrimaryKey
	private String fishName;
	private String imageUri;
	private String addedDate;

	public FishInCart(){}

	public FishInCart(String fishName,String imageUri, String addedDate) {

		this.imageUri = imageUri;
		this.fishName = fishName;
		this.addedDate = addedDate;
	}

	public String getFishName() {

		return fishName;
	}

	public String getAddedDate() {

		return addedDate;
	}

	public String getImageUri() {

		return imageUri;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		FishInCart that = (FishInCart) o;

		return fishName.equals(that.fishName);
	}

	@Override
	public int hashCode() {

		return fishName.hashCode();
	}
}
