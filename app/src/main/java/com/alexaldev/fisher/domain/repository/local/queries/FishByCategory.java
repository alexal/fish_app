package com.alexaldev.fisher.domain.repository.local.queries;

import com.alexaldev.fisher.domain.dataModel.Fish;
import com.alexaldev.fisher.domain.repository.local.RealmSpecification;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 *
 * Created by alexal on 2/12/2017.
 */
public class FishByCategory implements RealmSpecification<Fish> {

	private static FishByCategory INSTANCE = new FishByCategory();
	private String category;

	private FishByCategory(){}

	public static FishByCategory onCategory(String category){
		INSTANCE.category = category;
		return INSTANCE;
	}

	@Override
	public RealmResults<Fish> toRealmResults(Realm realm) {

		return realm.where(Fish.class)
				.equalTo("category",category)
				.findAll();

	}
}
