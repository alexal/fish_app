package com.alexaldev.fisher.domain.dataModel;

/**
 * Class describing an action that a user can make on a provided fish. This action may
 * be an "Add to cart" action or "Share".
 * Created by alexal on 17/12/2017.
 */

public class FishUserAction {

	public static final String ACTION_SHARE = "actn_sr";
	public static final String ACTION_ADD_CART = "actn_cart";
	public static final String ACTION_LIKE = "actn_like";

	private final String fishName;
	private final String  actionDescription;

	public FishUserAction(String fishName,
							 String actionDescription) {

		this.fishName = fishName;
		this.actionDescription = actionDescription;
	}

	public String getFishName() {

		return fishName;
	}

	public String getActionDescription() {

		return actionDescription;
	}
}
