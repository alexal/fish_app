package com.alexaldev.fisher.domain.dao;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.alexalbasemvp.mvp.BUS;
import com.alexaldev.fisher.domain.dataModel.Fish;
import com.alexaldev.fisher.domain.dataModel.FishInCart;
import com.alexaldev.fisher.domain.data_filters.ANDFilter;
import com.alexaldev.fisher.domain.data_filters.CategoryFilter;
import com.alexaldev.fisher.domain.data_filters.NameFilter;
import com.alexaldev.fisher.domain.data_filters.SeasonFilter;
import com.alexaldev.fisher.domain.repository.local.CartRepository;
import com.alexaldev.fisher.domain.repository.local.CartUpdateEvent;
import com.alexaldev.fisher.domain.repository.local.FishLocalRepository;
import com.alexaldev.fisher.domain.repository.local.queries.AllFishInCart;
import com.alexaldev.fisher.domain.repository.local.queries.FishByCategory;
import com.alexaldev.fisher.domain.repository.local.queries.FishByName;
import com.alexaldev.fisher.domain.repository.local.queries.FishByPrefix;
import com.alexaldev.fisher.domain.repository.local.queries.FishBySeason;
import com.alexaldev.fisher.domain.repository.local.queries.FishInCartByName;
import com.alexaldev.fisher.domain.repository.local.queries.AllFish;
import com.alexaldev.fisher.domain.repository.remote.RemoteRepository;
import com.alexaldev.fisher.domain.repository.remote.RemoteStateChangedEvent;
import com.alexaldev.fisher.domain.repository.remote.queries.AllRemoteFish;
import com.alexaldev.fisher.domain.data_filters.FilterValue;
import com.alexaldev.fisher.fish_presentation.FishViewedEvent;
import com.alexaldev.fisher.home.cart_view.DeleteFromCartEvent;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import org.greenrobot.eventbus.Subscribe;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import timber.log.Timber;

public class FishDAO implements ValueEventListener {

	//Remote repository state variables
	public static int REMOTE_STATE_CURRENT;
	public static final int REMOTE_STATE_DISCONNECTED = 1420;
	//TODO Syncing now is considered just the fetch iteration.
	public static final int REMOTE_STATE_SYNCING 	  = 1421;
	public static final int REMOTE_STATE_COMPLETED 	  = 1422;

	private static final int MAX_RECENT_VIEWED_CAPACITY = 3;

	private static final int LOCAL_STORAGE = 420;
	private static final int REMOTE_STORAGE = 421;

	//The repositories of the app
	private FishLocalRepository localRepository;
	private RemoteRepository remoteRepository;
	private CartRepository cartRepository;

	//Cache mechanisms
	private List<Fish> remoteFishCache;
	private ArrayBlockingQueue<String> recentSeenFish;

	//Single instance object
	private static FishDAO INSTANCE;

	public static FishDAO getInstance() {

		if (INSTANCE == null) {
			INSTANCE = new FishDAO();
		}
		return INSTANCE;
	}

	private FishDAO() {}

	/**
	 * Firstly, checks if the fish that should be exist in the local repo exist,
	 * and if not, it saves them. Also Initiates the remote repository connection and when
	 * the data are fetched they are cached locally.
	 */
	public void setup() {

		FishLocalRepository.configureLocalData();

		this.localRepository = FishLocalRepository.getInstance();
		this.remoteRepository = RemoteRepository.getInstance();
		this.cartRepository = CartRepository.Factory.create();

		this.remoteFishCache = new ArrayList<>();

		this.recentSeenFish = new ArrayBlockingQueue<>(MAX_RECENT_VIEWED_CAPACITY);

		for (String s :
				localRepository.getRecentViewedFishNames()) {
			recentSeenFish.offer(s);
		}

		BUS.subscribe(this);

		this.remoteRepository.queryAsync(AllRemoteFish.queryInstance(),
										 this);

		this.updateRemoteState(REMOTE_STATE_DISCONNECTED);

	}

	@Subscribe
	protected void onFishViewedEvent(FishViewedEvent event) {

		//Should add the fist to the recent ones only if available locally

		String fishName = event.getFishName();

		if (localRepository.query(
				FishByName.withName(fishName))
						   .size() > 0) {

			if (recentSeenFish.contains(fishName)) {
				//Do nothing, let it be

			} else {
				if (recentSeenFish.remainingCapacity() == 0) {
					//Should remove the head and add the new one in the tail
					recentSeenFish.poll();
					recentSeenFish.offer(fishName);
				} else {
					//Just add it
					recentSeenFish.offer(fishName);
				}
			}
			//TODO Add a cache mechanism
			localRepository.addRecentViewed(
					new ArrayList<>(recentSeenFish)
			);
		} else {
			Timber.d("Fish not available in local repo, skipping");
		}


	}

	@Subscribe
	protected void onRemoveFromCart(DeleteFromCartEvent event) {

		this.cartRepository.remove(
				cartRepository.query(FishInCartByName.withName(event.getFishName()))
							  .get(0)
		);

		BUS.postEvent(new CartUpdateEvent());
	}

	//-------------------------------FISH OBJECT ACCESS FUNCTIONS---------------------------------->

	public List<Fish> getAllFish() {

		List<Fish> result = new ArrayList<>(getAllFish(LOCAL_STORAGE));
		result.addAll(getAllFish(REMOTE_STORAGE));
		return result;
	}

	private List<Fish> getAllFish(int storageId) {

		switch (storageId) {
			case LOCAL_STORAGE:
				return localRepository.query(new AllFish());
			case REMOTE_STORAGE:
				return remoteFishCache;
			default:
				throw new IllegalArgumentException("Storage ID not valid");
		}
	}

	public int getAvailableFishCount(){
		return getAllFish().size();
	}

	public List<Fish> getFilteredFish(HashSet<FilterValue> filters) {

		if (filters == null ||
				filters.size() == 0) {
			//Return them all
			return getAllFish();
		} else {
			return new ArrayList<>(ANDFilter.instance().
					filterRecursive(getAllFish(),filters));
		}

	}

	public Fish getFishByName(String name) {

		//First lets try local repo
		List<Fish> result =
				this.localRepository.query(
						FishByName.withName(name)
				);

		if (result.size() != 1) {
			Timber.d("Tried fishByName query and got result size: %s",
					 result.size());
		} else {
			return result.get(0);
		}

		//If top failed, lets try the remote ones
		List<Fish> remoteResult = new NameFilter(name)
				.filter(remoteFishCache);

		if (remoteResult.size() != 1) {
			Timber.d("Tried fishByName query and got result size: %s ",
					 result.size());
		} else {
			return remoteResult.get(0);
		}

		throw new IllegalArgumentException("Fish name was not found in local or remote repo. WTF");
	}

	/**
	 * Get a new reference to the fish that were recently viewed by the user locally.
	 *
	 * @return a list with the recently viewed fish.
	 */
	public List<String> getRecentViewed() {

		return new ArrayList<>(recentSeenFish);
	}

	public boolean cartEntriesExist() {

		return this.cartRepository.cartEntriesExist();
	}

	/**
	 * Checks if the given fish can be added in the cart repository.
	 * Current configuration is that if the fish exists in the local repository, it can also
	 * be added in cart repository,otherwise not.
	 *
	 * @return
	 */
	public boolean fishCanBeAddedToCart(@NonNull Fish fish) {

		return localRepository.query(
				FishByName.withName(fish.getName()))
							  .size() > 0;
	}

	public boolean fishInCart(@NonNull Fish fish) {

		return (this.cartRepository.query(
				FishInCartByName.withName(fish.getName()))
								   .size() > 0);
	}

	public List<Fish> getFishByPrefix(String prefix){
		Timber.d("Searching fish for prefix");
		return localRepository.query(FishByPrefix.withPrefix(prefix));
	}

	public List<FishInCart> getAllFishInCart() {

		return new ArrayList<>(this.cartRepository.query(AllFishInCart.queryInstance()));
	}

	public void addToCart(Fish fish) {

		@SuppressLint("SimpleDateFormat")
		SimpleDateFormat format = new SimpleDateFormat("d/MM/yyyy");

		cartRepository.addItem(
				new FishInCart(fish.getName(),
							   fish.getImageUrl(),
							   format.format(Calendar.getInstance()
													 .getTime()))
		);

		BUS.postEvent(new CartUpdateEvent());
	}

	private void updateRemoteState(int state){
		REMOTE_STATE_CURRENT = state;
		BUS.postEvent(new RemoteStateChangedEvent(REMOTE_STATE_CURRENT));
	}

	//--------------------END OF FISH OBJECT ACCESS FUNCTIONS-------------------------------------->


	//FIREBASE CALLBACKS TO FETCH DATA FROM REMOTE REPO

	@Override
	public void onDataChange(DataSnapshot dataSnapshot) {

		//The first time this callback is triggered shoud inform the state
		updateRemoteState(REMOTE_STATE_SYNCING);

		for (DataSnapshot fishSnapshot : dataSnapshot.getChildren()) {

			Fish f = fishSnapshot.getValue(Fish.class);

			Timber.d("onDataChanged, fetched fish with ID: %s",
					 f.getName());

			this.remoteFishCache.add(f);
		}

		Timber.d("Finished fetching fish data. Cached data size: %s ",
				 remoteFishCache.size());

		updateRemoteState(REMOTE_STATE_COMPLETED);

	}

	@Override
	public void onCancelled(DatabaseError databaseError) {

		Timber.d("onCancelled with error: %s ",
				 databaseError.getMessage());
	}

}
