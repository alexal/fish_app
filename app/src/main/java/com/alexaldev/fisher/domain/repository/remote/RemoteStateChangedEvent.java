package com.alexaldev.fisher.domain.repository.remote;

/**
 * Event fired from the firebase each time it's state of connection changes.
 * For anyone interested, he should subscribe to the {@link com.alexalbasemvp.mvp.BUS}
 * to listen for the connection state of the remote repository.
 * Created by alexal on 10/12/2017.
 */

public class RemoteStateChangedEvent {

	private final int state;

	public RemoteStateChangedEvent(int state) {

		this.state = state;
	}

	public int getState() {

		return state;
	}
}
