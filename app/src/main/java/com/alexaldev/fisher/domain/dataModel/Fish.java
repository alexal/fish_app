package com.alexaldev.fisher.domain.dataModel;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 *  Fish model layer class. Used as the raw data model stored locally.
 *  @version 2.0
 *  @since 1.0
 *  @author alexaldev
 */

public class Fish extends RealmObject {

    @PrimaryKey
    private int fid;

    private String name;

    private String searchableName;

    private String imageUrl;

    private String description;

    private String cooking;

    private String price;

    private String availability;

    private String peopleCanEat;

    private String keepRange;

    private String season;

    private String category;

    private int likesCount;


    public int getID() {
        return fid;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getCooking() {
        return cooking;
    }

    public String getPrice() {
        return price;
    }

    public String getAvailability() {
        return availability;
    }

    public String getPeopleCanEat() {
        return peopleCanEat;
    }

    public String getKeepRange() {
        return keepRange;
    }

    public String getSeason() {
        return season;
    }

    public String getCategory() {
        return category;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getSearchableName() {

        return searchableName;
    }

    public int getLikesCount() {

        return likesCount;
    }

    public void addLike(){
    	this.likesCount++;
	}

	/**
     * Default constructor
     */
    public Fish(){
    }

    private Fish(Builder builder) {

        //Non null parameters
        this.fid = builder.UID;
        this.name = builder.name;

        if ( builder.imageUrl == null || builder.imageUrl.isEmpty() ){
            //TODO Default image url
            this.imageUrl = "default";
        }
        else {
            this.imageUrl = builder.imageUrl;
        }


        if (builder.description == null || builder.description.isEmpty()) {
            this.description = "Η περιγραφή λείπει";
        } else {
            this.description = builder.description;
        }

        if (builder.cooking == null || builder.cooking.isEmpty()) {
            this.cooking = "Διάφοροι τρόποι μαγειρέματος";
        } else {
            this.cooking = builder.cooking;
        }

        if (builder.price == null || builder.price.isEmpty()) {
            this.price = "Άγνωστη τιμή";
        } else {
            this.price = builder.price;
        }

        if (builder.availability == null || builder.availability.isEmpty()) {
            this.availability = "Βρίσκεται συχνά";
        } else {
            this.availability = builder.availability;
        }

        if (builder.peopleCanEat == null || builder.peopleCanEat.isEmpty()) {
            this.peopleCanEat = "Με 1 κιλό χορταίνουν 2-3 ενήλικες";
        } else {
            this.peopleCanEat = builder.peopleCanEat;
        }

        if (builder.keepRange == null) {
            this.keepRange = "Διατηρείται στο ψυγείο για 2-3 μέρες";
        } else {
            this.keepRange = builder.keepRange;
        }

        if (builder.season == null) {
            this.season = "Βρίσκεται όλον τον χρόνο";
        } else {
            this.season = builder.season;
        }

        if (builder.category == null) {
            this.category = "Μικρά ψάρια";
        } else {
            this.category = builder.category;
        }

        if (builder.searchableName == null)
            this.searchableName = GreekCapitalize.capitalizeWithoutTone(name.toCharArray());

		this.likesCount = 0;

    }


    /**
     * Builder class of the fish.
     */
    public static class Builder {

        private final int UID;
        private final String name;
        private String searchableName;
		private String imageUrl;
        private String description;
        private String cooking;
        private String price;
        private String availability;
        private String peopleCanEat;
        private String keepRange;
        private String season;
        private String category;

        public Builder(int UID, String name) {
            this.UID = UID;
            this.name = name;
        }

        public Builder setSearchableName(String searchableName){
            this.searchableName = searchableName;
            return this;
        }

        public Builder setImageUrl(String url) {
            this.imageUrl = url;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder setCooking(String cooking) {
            this.cooking = cooking;
            return this;
        }

        public Builder setPrice(String price) {
            this.price = price;
            return this;
        }

        public Builder setAvailability(String availability) {
          this.availability = availability;
            return this;
        }

        public Builder setPeopleCanEat(String peopleCanEat) {
            this.peopleCanEat = peopleCanEat;
            return this;
        }

        public Builder setKeepRange(String keepRange) {
            this.keepRange = keepRange;
            return this;
        }

        public Builder setSeason(String season) {
          	this.season = season;
            return this;
        }

        public Builder setCategory(String category) {
       		this.category = category;
            return this;
        }

        public Fish create() {
            return new Fish(this);
        }


    }


}
