package com.alexaldev.fisher.domain.data_filters;

import com.alexaldev.fisher.domain.dataModel.Fish;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by alexal on 2/12/2017.
 */

public class CategoryANDSeasonFilter implements DataFilter<Fish>{

	private static CategoryANDSeasonFilter INSTANCE = new CategoryANDSeasonFilter();
	private String category;
	private String season;

	private CategoryANDSeasonFilter(){}


	public static CategoryANDSeasonFilter withCategoryAndSeason(String category,
																String season){
		INSTANCE.category = category;
		INSTANCE.season = season;

		return INSTANCE;
	}

	@Override
	public List<Fish> filter(List<Fish> initialData) {
		return CategoryFilter.onCategory(category)
							 .filter(SeasonFilter.onSeason(season)
									.filter(initialData));

	}
}
