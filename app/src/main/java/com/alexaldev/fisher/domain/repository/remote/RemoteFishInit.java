package com.alexaldev.fisher.domain.repository.remote;

import com.alexaldev.fisher.domain.dataModel.Fish;
import com.alexaldev.fisher.domain.dataModel.FishAttrs;


import java.util.ArrayList;
import java.util.List;


public class RemoteFishInit {

	public static List<Fish> getEntries(){

		int counterId = 6;

		Fish.Builder builder = new Fish.Builder(++counterId,
												"Τσιπούρα ελευθέρας βοσκής")
				.setAvailability(FishAttrs.Availability.RARE)
				.setCategory(FishAttrs.Category.BIG_FISH)
				.setCooking(FishAttrs.Cooking.PSITO)
				.setDescription("Συγκριτικά με την τσιπούρα ιχθυοτροφείου είναι λιγότερο λιπαρή και συνήθως μεγαλύτερη. Την ξεχωρίζουμε από την πιο ανοιχτή απόχρωση του δέρματος της και την χρυσή λωρίδα που έχει στο πάνω μέρος του κεφαλιού της.Προσέχουμε τα μάτια να γυαλίζουν και τα βράγχια να έχουν έντονο χρώμα.")
				.setKeepRange(FishAttrs.KeepRange.DAYS_3_4)
				.setPeopleCanEat(FishAttrs.PeopleKiloRatio.PEOPLE_2_3)
				.setSeason(FishAttrs.Season.WINTER)
				.setImageUrl("https://firebasestorage.googleapis.com/v0/b/fisher-3e001.appspot.com/o/fish_images%2Ffish_tsipoura_ele.jpg?alt=media&token=de46f72a-b3d8-401f-8ee0-4dfdbd938767")
				.setPrice("12-25");

		Fish tsipoura_ele = builder.create();


		Fish.Builder builder1 = new Fish.Builder(++counterId,
												"Λιγδάκι")
				.setAvailability(FishAttrs.Availability.RARE)
				.setCategory(FishAttrs.Category.BIG_FISH)
				.setCooking(FishAttrs.Cooking.PSITO)
				.setDescription("Με την ονομασία λίγδα αναφερόμαστε στις μικρές τσιπούρες ελευθέρας βοσκής. Αποτελούν πολύ νόστιμο μεζέ, έχουν άσπρο και άπαχο κρέας. Προσέχουμε τα βράγχια τους να είναι κόκκινα και τα μάτια τους να μην είναι θαμπά.")
				.setKeepRange(FishAttrs.KeepRange.DAYS_3_4)
				.setPeopleCanEat(FishAttrs.PeopleKiloRatio.PEOPLE_2_3)
				.setSeason(FishAttrs.Season.WINTER)
				.setImageUrl("https://firebasestorage.googleapis.com/v0/b/fisher-3e001.appspot.com/o/fish_images%2Ffish_tsipoura_ele.jpg?alt=media&token=de46f72a-b3d8-401f-8ee0-4dfdbd938767")
				.setPrice("12-25");

		Fish ligdaki = builder1.create();

		Fish.Builder builder2 = new Fish.Builder(++counterId,
												 "Πέστροφα")
				.setAvailability(FishAttrs.Availability.USUAL)
				.setCategory(FishAttrs.Category.BIG_FISH)
				.setCooking(FishAttrs.Cooking.PSITO)
				.setDescription("Ένα ιδιαίτερο ψάρι με μαλακό κρέας του οποίου η εμφάνιση θυμίζει ψάρι γλυκού νερού. Οι περισσότερες πέστροφες που βρίσκουμε στην αγορά είναι καλλιεργημένες στην θάλασσα.Το κρέας της είναι ιδιαίτερα λιπαρό και προσφέρει πολλούς τρόπους μαγειρέματος.Προσέχουμε η \"γλίτσα\" του να είναι ορατή.")
				.setKeepRange(FishAttrs.KeepRange.DAYS_3_4)
				.setPeopleCanEat(FishAttrs.PeopleKiloRatio.PEOPLE_2_3)
				.setSeason(FishAttrs.Season.ALL_YEAR)
				.setImageUrl("https://firebasestorage.googleapis.com/v0/b/fisher-3e001.appspot.com/o/fish_images%2Ffish_pestrofa.jpg?alt=media&token=d5c91cfc-a2b1-47e6-804a-7c4e62fbaa1e")
				.setPrice("5-7");

		Fish pestrofa = builder2.create();

		List<Fish> result = new ArrayList<>();

		result.add(tsipoura_ele);result.add(ligdaki);result.add(pestrofa);

		return result;


	}
}
