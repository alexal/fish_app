package com.alexaldev.fisher.domain.repository.remote;


import android.support.annotation.NonNull;

import com.alexaldev.fisher.domain.dataModel.Fish;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class FirebaseBackend {

	//Declaration of the remote schema
	public static final String FISH_ENTRIES_CHILD = "fishEntries";

	private final DatabaseReference mDatabase;

	public static FirebaseBackend newInstance(){
		return new FirebaseBackend();
	}

	private FirebaseBackend(){
		this.mDatabase = FirebaseDatabase.getInstance().getReference();
	}

	/**
	 * Get the remote database reference.
	 * @return
	 */
	 DatabaseReference getRemoteDatabase(){
		return this.mDatabase;
	}


	/**
	 * Connects to the backend. Use this method to setup the backend
	 * from a presenter and disconnect() it when not needed.
	 */
	@Deprecated
	public void fetchAsync(@NonNull ValueEventListener eventListener){
		this.mDatabase.child(FISH_ENTRIES_CHILD).addListenerForSingleValueEvent(eventListener);
	}

	@Deprecated
	public void addFish(@NonNull Fish fish){

		if (this.mDatabase == null)
			throw new IllegalStateException("Database reference is null. Call fetchAsync() first");
		else {
			this.mDatabase.child(FISH_ENTRIES_CHILD).child(fish.getName()).setValue(fish);
		}
	}

}
