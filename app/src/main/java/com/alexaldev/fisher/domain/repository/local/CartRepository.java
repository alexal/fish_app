package com.alexaldev.fisher.domain.repository.local;

import android.util.Log;

import com.alexaldev.fisher.domain.dataModel.FishInCart;
import com.alexaldev.fisher.domain.repository.Repository;
import com.alexaldev.fisher.domain.repository.local.queries.AllFishInCart;
import com.alexaldev.fisher.domain.repository.local.queries.FishInCartByName;

import java.util.List;

import io.realm.Realm;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;


public class CartRepository implements Repository<FishInCart, RealmSpecification<FishInCart>> {

	@Override
	public void addItem(FishInCart item) {

		Log.d("CartRepo",
			  "Adding item" + item.getFishName());
		Realm realm = Realm.getDefaultInstance();
		realm.executeTransaction(realm1 -> {
			try {
				realm1.insert(item);
			}
			catch (RealmPrimaryKeyConstraintException e) {
				//Do nothing
			}

		});
		realm.close();
	}

	@Override
	public void addItems(Iterable<FishInCart> items) {

		Realm realm = Realm.getDefaultInstance();
		realm.executeTransaction(realm1 ->
										 realm1.copyToRealm(items));
		realm.close();
	}

	@Override
	public void update(FishInCart item) {

		Realm realm = Realm.getDefaultInstance();
		realm.executeTransaction(realm1 ->
										 realm1.copyToRealmOrUpdate(item));
		realm.close();

	}

	/**
	 * Checks if
	 *
	 * @return
	 */
	public boolean cartEntriesExist() {

		return query(
				AllFishInCart.queryInstance()).size() > 0;
	}

	@Override
	public void remove(FishInCart item) {

		Realm realm = Realm.getDefaultInstance();
		realm.executeTransaction
				(realm1 ->
				 {
					 List<FishInCart> toDelete = query(
							 FishInCartByName.withName(item.getFishName())
					 );

					 toDelete.get(0)
							 .deleteFromRealm();
				 }

				);
	}


	@Override
	public List<FishInCart> query(RealmSpecification<FishInCart> specification) {

		return specification.toRealmResults(
				Realm.getDefaultInstance()
		);
	}

	public static class Factory {

		public static CartRepository create() {

			return new CartRepository();
		}
	}
}
