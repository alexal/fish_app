package com.alexaldev.fisher.domain.repository.remote;

import android.support.annotation.NonNull;

import com.alexaldev.fisher.domain.dataModel.Fish;
import com.alexaldev.fisher.domain.repository.Repository;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import timber.log.Timber;


public class RemoteRepository implements Repository<Fish,FirebaseSpecification>{

	private FirebaseBackend backend;
	public static RemoteRepository INSTANCE;

	public static RemoteRepository getInstance(){
		if (INSTANCE == null)
			INSTANCE = new RemoteRepository();
		return INSTANCE;
	}


	private RemoteRepository(){
		this.backend = FirebaseBackend.newInstance();
	}



	@Override
	public void addItem(Fish item) {
		this.backend.addFish(item);
	}

	@Override
	public void addItems(Iterable<Fish> items) {
		throw new UnsupportedOperationException("Add each one noob.No you are the noob for not implementing " +
														"and writing this kind of stuff..Bless you");
	}

	@Override
	public void update(Fish item) {
		throw new UnsupportedOperationException("Me noob sit have you waiting.!.!!1");
	}

	@Override
	public void remove(Fish item) {
		throw new UnsupportedOperationException("Me noob sit have you waiting.!.!!1");
	}

	/**
	 * @deprecated Use {{@link #queryAsync(FirebaseSpecification, ValueEventListener)}}
	 * instead. All the remote queries MUST be called in an async form.
	 * @param specification
	 * @return
	 */
	@Override
	public List<Fish> query(FirebaseSpecification specification) {
		throw new UnsupportedOperationException("Currently remote data are fetched once and the " +
														"connection is shut down then." +
														"Use fetched data to perform queries on " +
														"them");
	}

	/**
	 * @param query
	 * @param listener
	 */
	public void queryAsync(FirebaseSpecification query,
								 @NonNull ValueEventListener listener){
		query.queryAsync(
				this.backend.getRemoteDatabase(),
				listener
		);
	}

	public static void uploadFishEntries(){

		for (Fish f : RemoteFishInit.getEntries()){
			getInstance().addItem(f);
		}

	}


}
