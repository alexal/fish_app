package com.alexaldev.fisher.domain.repository.local;

import com.alexaldev.fisher.domain.repository.Specification;

import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmResults;


public interface RealmSpecification<M extends RealmModel>
        extends Specification{

    RealmResults<M> toRealmResults(Realm realm);
}
