package com.alexaldev.fisher.domain.data_filters;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.alexaldev.fisher.domain.dataModel.Fish;
import com.alexaldev.fisher.domain.dataModel.FishAttrs;

import java.util.ArrayList;
import java.util.List;

/**
 * Parser class to create {@link DataFilter} instance based on a given {@link FilterValue} object.
 * Created by alexal on 8/12/2017.
 */

public class FilterValueParser {

	private FilterValueParser(){}

	public static List<DataFilter<Fish>> parseFrom(List<FilterValue> filterValues){

		List<DataFilter<Fish>> result = new ArrayList<>();

		for (FilterValue filterValue : filterValues){
			result.add(parseFrom(filterValue));
		}

		return result;
	}

	@Nullable
	public static DataFilter<Fish> parseFrom(@NonNull  FilterValue filterValue){
		switch (filterValue.getAttributeName()){
			case FishAttrs.Category.KEY:
				return CategoryFilter.onCategory(filterValue.getAttributeValue());
			case FishAttrs.Season.KEY:
				return SeasonFilter.onSeason(filterValue.getAttributeValue());
			default:
				return null;
		}
	}
}
