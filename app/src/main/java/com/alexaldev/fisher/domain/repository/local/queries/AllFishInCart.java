package com.alexaldev.fisher.domain.repository.local.queries;


import com.alexaldev.fisher.domain.dataModel.FishInCart;
import com.alexaldev.fisher.domain.repository.local.RealmSpecification;

import io.realm.Realm;
import io.realm.RealmResults;

public class AllFishInCart implements RealmSpecification<FishInCart>{

	private AllFishInCart(){}

	private static AllFishInCart INSTANCE = new AllFishInCart();

	public static AllFishInCart queryInstance(){
		return INSTANCE;
	}

	@Override
	public RealmResults<FishInCart> toRealmResults(Realm realm) {

		return realm.where(FishInCart.class).findAll();
	}
}
