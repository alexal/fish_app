package com.alexaldev.fisher.domain.data_filters;

import com.alexaldev.fisher.domain.dataModel.Fish;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * Created by alexal on 8/12/2017.
 */

public class ANDFilter implements DataFilter<Fish> {

	private static final ANDFilter INSTANCE = new ANDFilter();

	private ANDFilter(){}

	public static ANDFilter instance(){
		return INSTANCE;
	}

	public List<Fish> filterRecursive(List<Fish> initialData,
									  Set<FilterValue> filters){

		List<FilterValue> filterValues = new ArrayList<>(filters);

		return filterRecursive(initialData,
							   FilterValueParser.parseFrom(filterValues));

	}

	/**
	 * Helper function to apply many filters on a single dataset with a logical AND.
	 * This means that each filter is applied on the result of the previous one.
	 * I am not sure about the name of this method, cause i am a bit on albanian haze rn.
	 * Should check later for sure.
	 * @param initialData
	 * @param filters
	 * @return
	 */
	public List<Fish> filterRecursive(List<Fish> initialData,
									  List<DataFilter<Fish>> filters) {

		//TODO I am a bit high rn writing this, please excuse any really stupid implementation

		int filtersCount = filters.size();

		//Should only apply the first filter on the initial data. The others ones are applied in chain
		List<Fish> result = new ArrayList<>(
				filters.get(0).filter(initialData)
		);

		List<Fish> temp = new ArrayList<>();

		for (int i = 1; i < filtersCount; i++) {

			temp.addAll(result);
			result.clear();
			result.addAll(
					filters.get(i).
							filter(temp));
			temp.clear();
		}

		return result;
	}

	@Override
	public List<Fish> filter(List<Fish> initialData) {

		return null;
	}
}
