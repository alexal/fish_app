package com.alexaldev.fisher.domain.repository.local.queries;

import com.alexaldev.fisher.domain.dataModel.Fish;
import com.alexaldev.fisher.domain.repository.local.RealmSpecification;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;


public class FishByName implements RealmSpecification<Fish> {

	private String name;

	private static FishByName INSTANCE = new FishByName();

	private FishByName(){}

	public static FishByName withName(String name){
		INSTANCE.name = name;

		return INSTANCE;
	}

	@Override
	public RealmResults<Fish> toRealmResults(Realm realm) {

		return realm.where(Fish.class)
				.equalTo("name",this.name)
				.findAll();

	}
}
