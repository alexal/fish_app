package com.alexaldev.fisher.domain.repository.remote.queries;

import android.support.annotation.NonNull;

import com.alexaldev.fisher.domain.repository.remote.FirebaseSpecification;
import com.alexaldev.fisher.domain.repository.remote.FirebaseBackend;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

/**
 * Query for async fetch of all the fish in the remote repository.
 * Created by alexal on 8/12/2017.
 */
public class AllRemoteFish implements FirebaseSpecification {

	private static final AllRemoteFish INSTANCE = new AllRemoteFish();

	public static AllRemoteFish queryInstance(){
		return INSTANCE;
	}


	private AllRemoteFish(){}

	@Override
	public void queryAsync(@NonNull DatabaseReference dbReference,
						   @NonNull ValueEventListener fetchListener) {
		dbReference.child(FirebaseBackend.FISH_ENTRIES_CHILD)
				   .addListenerForSingleValueEvent(fetchListener);
	}
}
