package com.alexaldev.fisher.domain.data_filters;

import com.alexaldev.fisher.domain.dataModel.Fish;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexal on 9/11/2017.
 */

public class NameFilter implements DataFilter<Fish> {

	private final String nameFilter;

	public NameFilter(String nameFilter) {

		this.nameFilter = nameFilter;
	}

	@Override
	public List<Fish> filter(List<Fish> initialData) {

		ArrayList<Fish> result = new ArrayList<>();

		for (Fish f : initialData){
			if (f.getName().equals(nameFilter)){
				result.add(f);
			}
		}

		return result;
	}
}
