package com.alexaldev.fisher.domain.repository.local.queries;

import com.alexaldev.fisher.domain.dataModel.Fish;
import com.alexaldev.fisher.domain.dataModel.FishAttrs;
import com.alexaldev.fisher.domain.repository.local.RealmSpecification;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;


public class FishBySeason implements RealmSpecification<Fish> {

	private  String season;

	private static FishBySeason INSTANCE = new FishBySeason();

	public static FishBySeason withSeason(String season){
		INSTANCE.season = season;
		return INSTANCE;
	}

	@Override
	public RealmResults<Fish> toRealmResults(Realm realm) {

		RealmQuery<Fish> query = realm.where(Fish.class);

		query.equalTo("season",
					  season)
			 .or()
			 .equalTo("season",
					  FishAttrs.Season.ALL_YEAR);

		return query.findAll();
	}
}
