package com.alexaldev.fisher.domain.dao;


import com.alexaldev.fisher.domain.dataModel.Fish;

import java.util.List;

@Deprecated
public class RemoteDataFetched {

	private final List<Fish> fetchedData;

	public RemoteDataFetched(List<Fish> fetchedData) {

		this.fetchedData = fetchedData;
	}

	public List<Fish> getFetchedData() {

		return fetchedData;
	}
}
