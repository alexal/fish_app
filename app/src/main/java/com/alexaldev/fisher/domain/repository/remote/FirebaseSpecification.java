package com.alexaldev.fisher.domain.repository.remote;

import android.support.annotation.NonNull;

import com.alexaldev.fisher.domain.dataModel.Fish;
import com.alexaldev.fisher.domain.repository.Specification;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;


/**
 *
 */
public interface FirebaseSpecification extends Specification{

	void queryAsync(@NonNull DatabaseReference dbReference,
					@NonNull ValueEventListener fetchListener);
}
