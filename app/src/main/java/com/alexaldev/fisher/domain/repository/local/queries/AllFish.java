package com.alexaldev.fisher.domain.repository.local.queries;

import com.alexaldev.fisher.domain.dataModel.Fish;
import com.alexaldev.fisher.domain.repository.local.RealmSpecification;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 *
 * Created by alexal on 7/11/2017.
 */
public class AllFish implements RealmSpecification<Fish> {

	private static AllFish INSTANCE = new AllFish();

	public static AllFish queryInstance(){
		return INSTANCE;
	}

	@Override
	public RealmResults<Fish> toRealmResults(Realm realm) {

		RealmQuery<Fish> query = realm.where(Fish.class);

		return query.findAll();
	}
}
