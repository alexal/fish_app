package com.alexaldev.fisher.domain.dao;

/**
 * Event posted from {@link FishDAO} to inform anyone about the remote repository state.
 * Created by alexal on 12/12/2017.
 */

public class RemoteRepoStateChanged {

	private final int currentState;

	public RemoteRepoStateChanged(int currentState) {

		this.currentState = currentState;
	}

	public int getCurrentState() {

		return currentState;
	}
}
