package com.alexaldev.fisher.domain.data_filters;

import java.util.List;

/**
 * Created by alexal on 9/11/2017.
 */

public interface DataFilter<M> {

	List<M> filter(List<M> initialData);

}
