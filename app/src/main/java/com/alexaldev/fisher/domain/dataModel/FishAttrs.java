package com.alexaldev.fisher.domain.dataModel;

import java.util.Arrays;
import java.util.HashSet;

/**
 *
 */

public final class FishAttrs {

    private FishAttrs(){}

	public static final class Cooking{

		public static final String THGANITO = "Τηγανητό";
		public static final String PSITO = "Ψητό/Φούρνο";
		public static final String VRASTO = "Βραστό";

	}

	public static final class Availability {

    	public static final String RARE = "Βρίσκεται στην αγορά σπάνια";
    	public static final String USUAL = "Βρίσκεται στην αγορά συχνά";

	}

	public static final class PeopleKiloRatio{

    	public static final String PEOPLE_1_2 = "1-2 ενήλικες";
    	public static final String PEOPLE_2_3 = "2-3 ενήλικες";
    	public static final String PEOPLE_3_4 = "3-4 ενήλικες";

	}

	public static final class KeepRange{

    	public static final String DAYS_2_3 = "2-3 μέρες";
    	public static final String DAYS_3_4 = "3-4 μέρες";
    	public static final String DAYS_1_2 = "1-2 μέρες";

	}

	public static final class Season {

    	public static final String KEY = "Season";
    	public static final String SUMMER = "Εαρινή";
    	public static final String WINTER = "Χειμερινή";
    	public static final String ALL_YEAR = "Ετήσια";
	}

    public static final class Category {

    	public static final String KEY = "Category";
    	public static final String SMALL_FISH = "Μικρά";
    	public static final String BIG_FISH = "Μεγάλα";
    	public static final String MOLLUSC = "Μαλάκια";
    	public static final String FILLETS = "Φιλέτα";
    	public static final String SHELL = "Όστρακα";

    	public static HashSet<String> OPTIONS = new HashSet<>(
				Arrays.asList(SMALL_FISH,
							  BIG_FISH,
							  MOLLUSC,
							  FILLETS,
							  SHELL));
	}

}
