package com.alexaldev.fisher.domain.repository;

import java.util.List;

/**
 * Repository interface to be implemented by any class which is
 * gonna act like a Repository object pattern.
 */

public interface Repository<M,S extends Specification>  {

    void addItem(M item);

    void addItems(Iterable<M> items);

    void update(M item);

    void remove(M item);

    List<M> query(S specification);


}
