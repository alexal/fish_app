package com.alexaldev.fisher.domain.data_filters;

import com.alexaldev.fisher.domain.dataModel.Fish;
import com.alexaldev.fisher.domain.dataModel.FishAttrs;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 *
 * Created by alexal on 2/12/2017.
 */
public class CategoryFilter implements DataFilter<Fish>{

	private static CategoryFilter INSTANCE;
	private String category;



	private CategoryFilter(){}

	public static CategoryFilter onCategory(String category){
		if (INSTANCE == null){
			INSTANCE = new CategoryFilter();
		}

		INSTANCE.category = category;

		return INSTANCE;
	}

	@Override
	public List<Fish> filter(List<Fish> initialData) throws IllegalArgumentException{

		Timber.d("Filtering on category: %s",category );

		if (FishAttrs.Category.OPTIONS.contains(category)){

			ArrayList<Fish> result = new ArrayList<>();

			for (Fish f : initialData){
				if (f.getCategory().equals(category))
					result.add(f);
			}

			return result;
		}
		else {
			throw new IllegalArgumentException("Category specified is not recognized");
		}


	}
}
