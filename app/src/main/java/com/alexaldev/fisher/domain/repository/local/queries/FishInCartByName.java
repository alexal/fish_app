package com.alexaldev.fisher.domain.repository.local.queries;

import com.alexaldev.fisher.domain.dataModel.FishInCart;
import com.alexaldev.fisher.domain.repository.local.RealmSpecification;

import io.realm.Realm;
import io.realm.RealmResults;


public class FishInCartByName implements RealmSpecification<FishInCart> {

	private String fishName;

	private static FishInCartByName INSTANCE = new FishInCartByName();

	public static FishInCartByName withName(String fishName){
		INSTANCE.fishName = fishName;
		return INSTANCE;
	}

	@Override
	public RealmResults<FishInCart> toRealmResults(Realm realm) {

		return realm.where(FishInCart.class)
				.equalTo("fishName",fishName)
				.findAll();

	}
}
