package com.alexaldev.fisher.domain.data_filters;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Filter value
 * Created by alexal on 1/12/2017.
 */
public class FilterValue implements Parcelable {

	private final String attributeName;
	private final String attributeValue;

	public FilterValue(String attributeName, String attributeValue) {

		this.attributeName = attributeName;
		this.attributeValue = attributeValue;
	}

	public String getAttributeName() {

		return attributeName;
	}

	public String getAttributeValue() {

		return attributeValue;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		FilterValue that = (FilterValue) o;

		return attributeName != null ? attributeName.equals(that.attributeName) : that.attributeName == null;
	}



	@Override
	public int hashCode() {

		return attributeName != null ? attributeName.hashCode() : 0;
	}

	@Override
	public int describeContents() {

		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeString(this.attributeName);
		dest.writeString(this.attributeValue);
	}

	protected FilterValue(Parcel in) {

		this.attributeName = in.readString();
		this.attributeValue = in.readString();
	}

	public static final Parcelable.Creator<FilterValue> CREATOR = new Parcelable.Creator<FilterValue>() {

		@Override
		public FilterValue createFromParcel(Parcel source) {

			return new FilterValue(source);
		}

		@Override
		public FilterValue[] newArray(int size) {

			return new FilterValue[size];
		}
	};
}
