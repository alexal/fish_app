package com.alexaldev.fisher.domain.repository.local;



import com.alexal.baseutils.app_util.Prefs;
import com.alexaldev.fisher.R;
import com.alexaldev.fisher.domain.dataModel.Fish;
import com.alexaldev.fisher.domain.dataModel.FishAttrs;
import com.alexaldev.fisher.domain.repository.Repository;
import com.alexaldev.fisher.domain.repository.local.queries.AllFish;
import com.alexaldev.fisher.utils.ContentUtil;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.realm.Realm;
import timber.log.Timber;

public final class FishLocalRepository implements Repository<Fish, RealmSpecification<Fish>> {

	private static final FishLocalRepository INSTANCE = new FishLocalRepository();

	public static FishLocalRepository getInstance() {

		return INSTANCE;
	}

	private FishLocalRepository() {

	}

	@Override
	public void addItem(final Fish item) {

		Realm realm = Realm.getDefaultInstance();
		realm.executeTransaction(realm1 -> realm1.insert(item));
		realm.close();
	}


	@Override
	public void addItems(final Iterable<Fish> items) {

		Realm realm = Realm.getDefaultInstance();

		realm.executeTransaction(realm1 -> realm1.copyToRealm(items));

		realm.close();

	}


	@Override
	public void update(final Fish item) {

		Realm realm = Realm.getDefaultInstance();
		realm.executeTransaction(realm1 -> realm1.insert(item));
		realm.close();
	}

	@Override
	public void remove(final Fish item) {

		Realm.getDefaultInstance()
			 .executeTransaction(realm -> {

			 });
	}

	@Override
	public List<Fish> query(RealmSpecification<Fish> specification) {
        return specification
				.toRealmResults(Realm.getDefaultInstance());
	}

	/**
	 * Create the list with the fish data which can be available offline.
	 * You should call this method only once to initialize the database.
	 *
	 * @return
	 */
	public static List<Fish> getLocallyAvailableProducts() {

		int counterId = 0;

		Fish.Builder builder = new Fish.Builder(++counterId,
												"Γαύρος")
				.setAvailability(FishAttrs.Availability.USUAL)
                .setCategory(FishAttrs.Category.SMALL_FISH)
				.setCooking(FishAttrs.Cooking.THGANITO)
				.setDescription("Αποτελέι το πιο κλασικό και προσβάσιμο ψάρι το καλοκαίρι. Οι πληθυσμοί του στις ελληνικές θάλασσες είναι πολύ μεγάλες.Σε αυτό το γεγονός οφείλεται η χαμηλή τιμή του.Το κρέας του θεωρείται πάρα πολύ νόστιμο.Προσέχουμε το μάτι του να μην είναι κόκκινο, και να μην διαλύεται στο χέρι.")
				.setKeepRange(FishAttrs.KeepRange.DAYS_1_2)
				.setPeopleCanEat(FishAttrs.PeopleKiloRatio.PEOPLE_2_3)
				.setSeason(FishAttrs.Season.SUMMER)
				.setImageUrl(ContentUtil.getResourceUri(R.drawable.fish_gauros).toString())
				.setPrice("3-6");

		Fish gauros = builder.create();

		Fish.Builder builder1 = new Fish.Builder(++counterId,"Τσιπούρα ιχθυοτροφείου")
				.setAvailability(FishAttrs.Availability.USUAL)
				.setCategory(FishAttrs.Category.BIG_FISH)
				.setCooking(FishAttrs.Cooking.PSITO)
				.setDescription("Η τσιπούρα αποτελέι ένα απο τα πιο κλασικά γεύματα στην Ελλάδα, λόγω του εύγεστου κρέατος και της μεγάλης παραγωγής στις ελληνικές θάλασσες. Προσέχουμε το μάτι να γυαλίζει και τα βράγχια να έχουν έντονο  κόκκινο χρώμα.")
				.setKeepRange(FishAttrs.KeepRange.DAYS_2_3)
				.setPeopleCanEat(FishAttrs.PeopleKiloRatio.PEOPLE_1_2)
				.setSeason(FishAttrs.Season.ALL_YEAR)
				.setImageUrl(ContentUtil.getResourceUri(R.drawable.fish_tsipoura_ixth).toString())
				.setPrice("7-12");

		Fish tsipoura = builder1.create();

		Fish.Builder builder2 = new Fish.Builder(++counterId,"Σαρδέλα")
				.setAvailability(FishAttrs.Availability.USUAL)
				.setCategory(FishAttrs.Category.SMALL_FISH)
				.setCooking(FishAttrs.Cooking.THGANITO)
				.setDescription("Αποτελεί το είδος με τον μεγαλύτερο πληθυσμό στις ελληνικές θάλασσες.Οι ποσότητες του το καλοκαίρι είναι τεράστιες, έτσι βρίσκεται πολύ εύκολα στην αγορά. Το κρέας του είναι πολύ γευστικό και υπάρχουν πάρα πολλές συνταγές. Προσέχουμε να μην είναι κόκκινο το μάτι του και να μην διαλύεται στο χέρι.")
				.setKeepRange(FishAttrs.KeepRange.DAYS_1_2)
				.setPeopleCanEat(FishAttrs.PeopleKiloRatio.PEOPLE_2_3)
				.setSeason(FishAttrs.Season.SUMMER)
				.setImageUrl(ContentUtil.getResourceUri(R.drawable.fish_sardela).toString())
				.setPrice("3-6");

		Fish sardela = builder2.create();

		Fish.Builder builder3 = new Fish.Builder(++counterId,"Σολομός")
				.setAvailability(FishAttrs.Availability.USUAL)
				.setCategory(FishAttrs.Category.FILLETS)
				.setCooking(FishAttrs.Cooking.PSITO)
				.setDescription("Αποτελεί ένα από τα πιο δημοφιλή είδη φιλέτου. Είναι πλούσιο σε Ω3 και υπάρχουν πολλοί τρόπου μαγειρέματος. Το κρέας τους είναι αρκετά λιπαρό, οπότε καλό είναι να αποφεύγεται να μπαίνει στο τηγάνι. Προσέχουμε το χρώμα των φιλέτων να είναι έντονα κόκκινο.")
				.setKeepRange(FishAttrs.KeepRange.DAYS_1_2)
				.setPeopleCanEat(FishAttrs.PeopleKiloRatio.PEOPLE_2_3)
				.setSeason(FishAttrs.Season.SUMMER)
				.setImageUrl(ContentUtil.getResourceUri(R.drawable.fish_solomos).toString())
				.setPrice("3-6");

		Fish solomos = builder3.create();

		Fish.Builder builder4 = new Fish.Builder(++counterId,"Μπακαλιάρος")
				.setAvailability(FishAttrs.Availability.USUAL)
				.setCategory(FishAttrs.Category.BIG_FISH)
				.setCooking(FishAttrs.Cooking.VRASTO)
				.setDescription("Αποτελέι πολύ νόστιμο και ουδέτερο ψάρι. Μπορεί να μαγειρευτεί με πολλούς τρόπους και είναι και ιδιαίτερα ευχάριστο και για τα παιδιά. Το μέγεθος του ποικίλει αρκετά. Προσέχουμε το μάτι να είναι έντονο, να μην είναι πολύ μαλακό το κρέας του και τα βράγχια να έχουν έντονο χρώμα.")
				.setKeepRange(FishAttrs.KeepRange.DAYS_2_3)
				.setPeopleCanEat(FishAttrs.PeopleKiloRatio.PEOPLE_2_3)
				.setSeason(FishAttrs.Season.WINTER)
				.setImageUrl(ContentUtil.getResourceUri(R.drawable.fish_mpakaliaros).toString())
				.setPrice("8-15");

		Fish mpakaliaros = builder4.create();

		Fish.Builder builder5 = new Fish.Builder(++counterId,"Μύδια")
				.setAvailability(FishAttrs.Availability.USUAL)
				.setCategory(FishAttrs.Category.SHELL)
				.setCooking(FishAttrs.Cooking.VRASTO)
				.setDescription("Το μύδι αποτελεί το πιο γνωστό είδος όστρακου καθώς καλλιεργείται. Αποτελεί πολύ καλή οικονομική λύση για ωραία πιάτα, με μια σχετικά ουδέτερη γεύση. Αν αγοράσουμε μύδια με κέλυφος προσέχουμε να βγάζουμε την τρίχα που ξεκινάει από το εσωτερικό του μυδιού. Εύκολα το αφαιρούμε όταν τα χουμε ζεστάνει. Τα μύδια που έρχονται συσκευασμένα μπορούν να συντηρηθούν στο ψυγείο έως και 5 μέρες από την αναγραφόμενη ημερομηνία. ")
				.setKeepRange(FishAttrs.KeepRange.DAYS_3_4)
				.setPeopleCanEat(FishAttrs.PeopleKiloRatio.PEOPLE_2_3)
				.setSeason(FishAttrs.Season.ALL_YEAR)
				.setImageUrl(ContentUtil.getResourceUri(R.drawable.fish_midia).toString())
				.setPrice("2-4");

		Fish mydia = builder5.create();

		List<Fish> result = new ArrayList<>();

		result.add(gauros);result.add(sardela);result.add(tsipoura);result.add(solomos);result.add(mpakaliaros);result.add(mydia);

		return result;
	}

	/**
	 * Firstly checks if the default local fish data exist in the local repository.
	 * If not, it saves them locally.
	 */
	 public static void configureLocalData(){

		//Check if the fish entries that should exist in database exist

		FishLocalRepository repository = getInstance();

		List<Fish> fishData = repository
									 .query(new AllFish());

		if (fishData != null && fishData.size() > 0){
			//Do nothing, fish exist
			Timber.d("Fish data exist, not writing");
		}
		else {
			Timber.d("Fish data do not exist, writing data...");
			for (Fish f : getLocallyAvailableProducts()){
				repository.addItem(f);
			}
		}

	}

	/**
	 * Stores the given fish list as recent.
	 */
	public void addRecentViewed(List<String> recentViewedFish){

		Prefs.edit().putStringSet(
				Prefs.PREF_RECENT_VIEWED, new HashSet<>(recentViewedFish)
		).apply();
	}

	public Set<String> getRecentViewedFishNames(){
		return new HashSet<>(Prefs.prefs()
								  .getStringSet(Prefs.PREF_RECENT_VIEWED,
												new HashSet<>(0)));
	}


	public static void deleteLocalData(){
		Realm.getDefaultInstance().delete(Fish.class);

	}


}
