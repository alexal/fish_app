package com.alexaldev.fisher.domain.data_filters;

import com.alexaldev.fisher.domain.dataModel.Fish;
import com.alexaldev.fisher.domain.dataModel.FishAttrs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import timber.log.Timber;


public class SeasonFilter implements DataFilter<Fish> {

	private static final HashSet<String> acceptedSeason = new HashSet<>(Arrays.asList(
			FishAttrs.Season.ALL_YEAR,
			FishAttrs.Season.SUMMER,
			FishAttrs.Season.WINTER));

	private String seasonToFilter;

	private static final SeasonFilter INSTANCE = new SeasonFilter();

	private SeasonFilter(){}

	public static SeasonFilter onSeason(String seasonToFilter){
		if (acceptedSeason.contains(seasonToFilter)){
			INSTANCE.seasonToFilter = seasonToFilter;
			return INSTANCE;
		}
		else {
			throw new IllegalArgumentException("Season filter was not found.");
		}

	}

	@Override
	public List<Fish> filter(List<Fish> initialData) {

		Timber.d("Filtering on season: %s",seasonToFilter);

		ArrayList<Fish> result = new ArrayList<>();

		for (Fish f : initialData) {
			if (f.getSeason()
				 .equals(seasonToFilter) ||
					f.getSeason().equals(FishAttrs.Season.ALL_YEAR)) {
				result.add(f);
			}
		}
		return result;
	}
}
