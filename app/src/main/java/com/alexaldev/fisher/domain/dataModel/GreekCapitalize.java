package com.alexaldev.fisher.domain.dataModel;

import java.util.HashMap;

/**
 * Helper class with methods which capitalize greek characters
 * without accepting any tonos.
 * Created by alexal on 15/12/2017.
 */

public class GreekCapitalize {

	private static final HashMap<Character,Character> SMALL_TONED_TO_ATONED_CAPITAL;

	static {
		SMALL_TONED_TO_ATONED_CAPITAL = new HashMap<>();
		SMALL_TONED_TO_ATONED_CAPITAL.put((char)940,(char)913);
		SMALL_TONED_TO_ATONED_CAPITAL.put((char)941,(char)917);
		SMALL_TONED_TO_ATONED_CAPITAL.put((char)942,(char)919);
		SMALL_TONED_TO_ATONED_CAPITAL.put((char)943,(char)921);
		SMALL_TONED_TO_ATONED_CAPITAL.put((char)972,(char)927);
		SMALL_TONED_TO_ATONED_CAPITAL.put((char)973,(char)933);
		SMALL_TONED_TO_ATONED_CAPITAL.put((char)974,(char)937);
	}

	private GreekCapitalize(){}

	public static String capitalizeWithoutTone(char letter){
		return String.valueOf(SMALL_TONED_TO_ATONED_CAPITAL.get(letter));
	}

	/**
	 *
	 * @param letters
	 * @return
	 */
	public static String capitalizeWithoutTone(char[] letters){

		char[] atonedCapital = new char[letters.length];

		for (int i = 0; i < letters.length; i++) {
			if (SMALL_TONED_TO_ATONED_CAPITAL.containsKey(letters[i]))
				atonedCapital[i] = SMALL_TONED_TO_ATONED_CAPITAL.get(letters[i]);
			else
				atonedCapital[i] = String.valueOf(letters[i]).toUpperCase().toCharArray()[0]; // <-- This is aweful man..
		}

		return String.valueOf(atonedCapital);

	}

}
