package com.alexaldev.fisher.domain.repository.local;



/**
 * Event fired when an update in the cart entries take place.
 * Update means either add or remove functionality.
 * You should fetch again the cart after this event takes place
 * and update any UI needed.
 * Created by alexal on 19/11/2017.
 */

public class CartUpdateEvent {

}
