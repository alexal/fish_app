package com.alexalbasemvp.mvp;

import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.NonNull;

/**
 * Presenter layer of MVP architecture.
 * @param <V> The view the presenter is related with.
 */
public abstract class BasePresenter<V extends MvpView> implements MvpPresenter {

	private V mView;

	void attachView(@NonNull V view){

		this.mView = view;
		this.onAttach(this.mView);

		if (this.subscribesToEventBus())
			BUS.subscribe(this);
	}

	void detachView(){

		if (this.subscribesToEventBus())
			BUS.unsubscribe(this);

		this.onDetach();

		this.mView = null;
	}

	protected V getView(){

		return this.mView;
	}

	/**
	 * Called when a new intent occurs from the view, or an intent in creation.
	 * If you want to initialize your logic from a starting intent, use this method instead of
	 * onAttach() where you do not have access to the intent.
	 * WARNING: If you want to handle the intent on creation you must preserve the presenter
	 * by calling {@link BaseController#preservePresenter()}.
	 * @param intent the coming intent to the view, or the intent on creation.
	 */
	protected void onComingIntent(Intent intent){}


	protected Resources appResources(){
		return this.mView.appResources();
	}

	protected void showMessage(String message){
		getView().showMessage(message);
	}

	protected abstract boolean subscribesToEventBus();

	protected abstract void onAttach(@NonNull V view);

	protected abstract void onDetach();

}
