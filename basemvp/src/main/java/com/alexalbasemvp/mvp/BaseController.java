package com.alexalbasemvp.mvp;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;

import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import butterknife.ButterKnife;

public abstract class BaseController<V extends MvpView,P extends BasePresenter<V>>
		extends AppCompatActivity implements MvpView{

	private P mPresenter;

	@Override
	protected void onCreate(@Nullable final Bundle pSavedInstanceState) {
		super.onCreate(pSavedInstanceState);

		this.setContentView(this.getLayoutId());

		ButterKnife.bind(this);

		//Set toolbar
		final Toolbar toolbar = this.getToolbar();

		if (toolbar != null) {
			this.setSupportActionBar(toolbar);
			toolbar.setNavigationOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					onBackPressed();
				}
			});
		}
		final ActionBar ab = this.getSupportActionBar();
		if (ab != null) {

			ab.setDisplayHomeAsUpEnabled(this.homeAsUpEnabled());
		}

		this.setTitle(this.getToolbarTitle());


		if (this.preservePresenter())
			this.setupPresenter(getIntent());


		this.initViews();

	}

	protected abstract void initViews();


	@Override
	protected void onDestroy() {

		if (this.preservePresenter()){
			this.mPresenter.detachView();
			this.mPresenter = null;
		}

		super.onDestroy();
	}

	protected abstract V getThis();

	protected abstract P newPresenter();

	@Override
	public void onStart() {
		super.onStart();

		if (!this.preservePresenter()){
			this.setupPresenter(null);
		}
	}

	@Override
	protected void onStop() {

		if (!this.preservePresenter()){
			this.mPresenter.detachView();
			this.mPresenter = null;

		}

		super.onStop();
	}


	private void setupPresenter(@Nullable Intent intent){
		this.mPresenter = this.newPresenter();
		this.mPresenter.attachView(this.getThis());

		if (intent != null)
			this.mPresenter.onComingIntent(intent);
	}

	@Override
	protected void onNewIntent(Intent intent) {

		super.onNewIntent(intent);

		this.mPresenter.onComingIntent(intent);
	}

	@Override
	public Resources appResources() {

		return this.getResources();
	}

	/**
	 * Commits a replace transaction on the given fragment. If you want to use
	 * this utility, make sure to override {@link #getFragmentContainer()}
	 * to set the ID of the FRAGMENT_CONTAINER.
	 * @param fragment
	 */
	protected void replaceTransaction(BaseView<P> fragment){

		getSupportFragmentManager().beginTransaction()
								   .replace(getFragmentContainer(),fragment)
								   .commit();
	}

	protected void replaceTransaction(BaseView<P> fragmentView,
													int transitionId){
		getSupportFragmentManager().beginTransaction()
								   .replace(getFragmentContainer(),fragmentView)
								   .setTransition(transitionId)
								   .commit();
	}

	protected void addTransaction(BaseView<P> fragmentView,
								  int transitionId){

		getSupportFragmentManager().beginTransaction()
								   .add(getFragmentContainer(),fragmentView)
								   .setTransition(transitionId)
								   .commit();
	}

	protected void addTransactionInBackstack(BaseView<P> fragmentView,
											 @Nullable String tag,
											 int transitionId){
		getSupportFragmentManager().beginTransaction()
								   .add(getFragmentContainer(),fragmentView)
								   .addToBackStack(tag)
								   .setTransition(transitionId)
								   .commit();
	}

	protected void addTransaction(BaseView<P> fragmentView){

	}

	/**
	 * Commits a replace transaction on the given fragment adding it to
	 * the backstack. If you want to use
	 * this utility, make sure to override {@link #getFragmentContainer()}
	 * to set the ID of the FRAGMENT_CONTAINER.
	 * @param fragmentView
	 */

	protected void replaceTransactionWithBackstack(BaseView<P> fragmentView){

		getSupportFragmentManager().beginTransaction()
								   .replace(getFragmentContainer(),fragmentView)
								   .addToBackStack(fragmentView.getTag())
								   .commit();
	}

	@Override
	public void showMessage(String message) {

		Toast.makeText(this,
					   message,
					   Toast.LENGTH_SHORT)
			 .show();
	}

	@Override
	public void showMessageWithAction(String message, String actionMessage,
									  View.OnClickListener actionListener) {
		  throw new UnsupportedOperationException("Override this method in your view implementation");
	}

	@IdRes
	protected int getFragmentContainer(){
		return 0;
	}

	protected P getPresenter(){
		return this.mPresenter;
	}


	protected boolean preservePresenter(){
		return false;
	}

	/**
	 * Reference to the layout.
	 *
	 * @return
	 */
	@LayoutRes
	protected abstract int getLayoutId();

	/**
	 * Reference to the toolbar.
	 *
	 * @return
	 */
	@Nullable
	protected abstract Toolbar getToolbar();

	/**
	 * The title in the toolbar.
	 *
	 * @return
	 */
	protected abstract String getToolbarTitle();


	/**
	 * Add the back button.
	 *
	 * @return
	 */
	protected abstract boolean homeAsUpEnabled();



}
