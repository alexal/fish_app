package com.alexalbasemvp.mvp;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseView<P extends MvpPresenter>
		extends Fragment implements MvpView{

	private P mPresenter;
	protected Unbinder unbinder;

	public P getPresenter() {

		return mPresenter;
	}

	public void setPresenter(P presenter){
		if (this.mPresenter == null)
			this.mPresenter = presenter;
		else {
			throw new IllegalStateException("The presenter is already set. Are you sure you want to reset the reference?");
		}
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
							 @Nullable Bundle savedInstanceState) {

		View root = inflater.inflate(this.getLayoutId(),
									 container,
									 false);

		unbinder = ButterKnife.bind(this,root);

		return root;

	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

		super.onViewCreated(view,
							savedInstanceState);

		getActivity().setTitle(this.getToolbarTitle());

		this.initViews(view);
	}

	@Override
	public void onDestroyView() {

		this.unbinder.unbind();

		super.onDestroyView();
	}

	@Override
	public void onDestroy() {

		mPresenter = null;

		super.onDestroy();
	}

	@Override
	public void showMessageWithAction(String message, String actionMessage,
									  View.OnClickListener actionListener) {
		throw new UnsupportedOperationException("Override this method in your view implementation");
	}

	@Override
	public Resources appResources() {

		return getResources();
	}

	@Override
	public void showMessage(String message) {

		Toast.makeText(getContext(),
					   message,
					   Toast.LENGTH_SHORT)
			 .show();
	}

	@LayoutRes
	protected abstract int getLayoutId();

	@Nullable
	protected abstract String getToolbarTitle();

	/**
	 * Make any necessary view handling here. The view has been created.
	 * @param root the root view of the fragment.
	 */
	protected void initViews(View root){

		//Configure the home indicator
		try {

			if (getCustomHomeIndicator() != 0){
				AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
				if (appCompatActivity.getSupportActionBar() != null)
					appCompatActivity.getSupportActionBar().setHomeAsUpIndicator(getCustomHomeIndicator());
			}
		}
		catch (ClassCastException e){
			//Fail silently, the home indicator just won't work
		}


	}

	@DrawableRes
	protected int getCustomHomeIndicator(){
		return 0;
	}





}
