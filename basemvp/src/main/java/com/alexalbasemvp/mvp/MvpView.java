package com.alexalbasemvp.mvp;

import android.content.res.Resources;
import android.view.View;

public interface MvpView {

	Resources appResources();

	void showMessage(String message);

	void showMessageWithAction(String message,
							   String actionMessage,
							   View.OnClickListener actionListener);
}
