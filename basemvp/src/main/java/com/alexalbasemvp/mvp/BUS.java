package com.alexalbasemvp.mvp;

import org.greenrobot.eventbus.EventBus;

public class BUS {

	protected static EventBus BUS = EventBus.getDefault();

	private BUS(){}

	public static void subscribe(Object subscriber){
		BUS.register(subscriber);
	}

	public static void unsubscribe(Object subsriber){
		BUS.unregister(subsriber);
	}

	public static void postEvent(Object event){
		BUS.post(event);
	}


}
